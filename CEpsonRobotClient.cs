﻿using BGI.Common.Comm;
using BGI.Common.Config;
using BGI.Common.Logging;
using BGI.RobotIntegrationService.Protocol;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BGI.RobotIntegrationService
{    
    public class CEpsonRobotClient
    {
        private string robotID = string.Empty;
        private CConnectedSocket linkSocket;
        private object _LockObj = new object();
        private Logger _Logger = LogMgr.GetLogger("CEpsonRobotClient");

        public delegate void DeviceConnectHandler(object send, object data);
        public delegate void ReceivedDataHandler(CEpsonRobotData data);
        public event ReceivedDataHandler ReceivedDataEvent;

        public CEpsonRobotClient(CConnectedSocket rClient)
        {
            linkSocket = rClient;
            linkSocket.ReceiveDataCompleteEvent += AsySocket_ReceiveDataCompleteEvent;
                        
            //_Logger.SetMinLogLevel(LogSeverityEnum.Info);
            _Logger.SetMinLogLevel(ConfigMgr.Get.GetCurValue<LogSeverityEnum>("System", "LogLevel"));
        }

        public void Close()
        {
            linkSocket.Close();
        }

        public bool sentCmd2Robot(string rCmd)
        {
            _Logger.Log()(LogSeverityEnum.Info, string.Format("{0}-{1}", robotID, rCmd));
            return linkSocket.SendData(rCmd + CEpsonRobotProtocol.Terminator);
        }

        public int sentCmd2Robot(string rCmd, out Dictionary<string, string> cmdQ, Dictionary<string, string> parameters = null, int timeout = 60000)
        {
            int ret = 0;
            cmdQ = new Dictionary<string, string>();
            try
            {
                cmdQ = CEpsonRobotProtocol.GetCmdQ(rCmd, parameters);
                if (cmdQ.Count == 0) throw new Exception(string.Format("{0}:GetCmdQ Failed!", rCmd));
                deleteItems(cmdQ.Keys.ToArray(), ref recvCmdRespDict);

                var cmds = cmdQ.Keys.ToArray();
                foreach (var cmd in cmds)
                {
                    _Logger.Log()(LogSeverityEnum.Info, string.Format("{0}-{1}", robotID,cmd));
                    linkSocket.SendData(cmd + CEpsonRobotProtocol.Terminator);
                    if (!waitCmdResp(cmd, ref cmdQ, timeout))
                    {
                        ret = -1;
                        break;
                    }
                    _Logger.Log()(LogSeverityEnum.Info, string.Format("{0}-{1}-{2}", robotID, cmd, cmdQ[cmd]));
                }

                // Check cmd result
                if (ret == 0) ret = CEpsonRobotProtocol.CheckCmdResult(rCmd, cmdQ);
            }
            catch (Exception ex)
            {
                ret = -999;
                _Logger.Log()(LogSeverityEnum.Fatal, ex.ToString());
            }
            finally
            {
                if(cmdQ.Count > 0) deleteItems(cmdQ.Keys.ToArray(), ref recvCmdRespDict);
            }

            return ret;
        }
        public int sentCmd2Robot(ref Dictionary<string, string> cmdQ)
        {
            int ret = 0;
            try
            {
                deleteItems(cmdQ.Keys.ToArray(), ref recvCmdRespDict);

                var cmds = cmdQ.Keys.ToArray();
                foreach (var cmd in cmds)
                {
                    _Logger.Log()(LogSeverityEnum.Info, string.Format("{0}-{1}", robotID, cmd));
                    linkSocket.SendData(cmd + CEpsonRobotProtocol.Terminator);
                    if (!waitCmdResp(cmd, ref cmdQ))
                    {
                        ret = -1;
                        break;
                    }
                    _Logger.Log()(LogSeverityEnum.Info, string.Format("{0}-{1}-{2}", robotID, cmd, cmdQ[cmd]));
                }

                // Check cmd result
                if  (ret == 0) ret = CEpsonRobotProtocol.CheckCmdResult(cmdQ);
            }
            catch (Exception ex)
            {
                ret = -999;
                _Logger.Log()(LogSeverityEnum.Fatal, ex.ToString());
            }
            finally
            {
                if (cmdQ.Count > 0) deleteItems(cmdQ.Keys.ToArray(), ref recvCmdRespDict);
            }

            return ret;
        }

        private bool waitCmdResp(string cmd, ref Dictionary<string, string> cmdQ, int timeout = 60000)
        {
            bool ret = false;
            try
            {
                int times = 0;
                ushort intv = 100;
                int cnt = timeout / intv;
                do
                {
                    if (recvCmdRespDict.ContainsKey(cmd))
                    {
                        ret = true;
                        cmdQ[cmd] = recvCmdRespDict[cmd];
                        break;
                    }

                    times++;
                    Thread.Sleep(intv);
                } while (times < cnt);
                if(!ret) _Logger.Log()(LogSeverityEnum.Info, string.Format("{0}-{1}-Timeout({2})-BufferInSnapShot({3})", robotID, cmd, timeout, recvCmdResp));
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Fatal, ex.ToString());
            }

            return ret;
        }

        private string recvCmdResp;
        private Dictionary<string, string> recvCmdRespDict = new Dictionary<string, string>();
        private void deleteItems(string[] itemKeys,ref Dictionary<string, string> itemDict)
        {
            lock (itemDict)
            {
                foreach (var key in itemKeys) itemDict.Remove(key);
            }
        }

        public string RobotID
        {
            get
            {
                return robotID;
            }
        }

        public string RobotIP
        {
            get
            {
                return linkSocket.RemoteIP;
            }
        }

        public void AsySocket_ReceiveDataCompleteEvent(System.Net.Sockets.Socket clientsocket, string receiveData)
        {
            try
            {
                lock (_LockObj)
                {
                    List<string> datas;
                    recvCmdResp += receiveData;
                    //_Logger.Log()(LogSeverityEnum.Info, recvCmdResp);
                    CEpsonRobotProtocol.ParseCmdResp(ref recvCmdResp, ref recvCmdRespDict, out datas);
                    if (datas.Count > 0)
                    {
                        EEpsonRobotData dataType;
                        string strContent;
                        foreach (var d in datas)
                        {                                
                            dataType = CEpsonRobotProtocol.ParseData(d, out strContent);
                            if (dataType == EEpsonRobotData.Unknow)
                            {
                                if (d.Contains("connect"))
                                {
                                    robotID = d.Substring(0, 2);
                                    //if (eventDeviceConnect != null)
                                    //    eventDeviceConnect.Invoke(RobotIP, robotID);
                                }
                                _Logger.Log()(LogSeverityEnum.Info, "Data receive: {0}", d);
                            }
                            else
                            {
                                if (robotID != string.Empty)
                                {
                                    if (dataType == EEpsonRobotData.IOStates || dataType == EEpsonRobotData.MIOStates || dataType == EEpsonRobotData.POS || dataType == EEpsonRobotData.RecycleSensorEnable || dataType == EEpsonRobotData.RecycleSlotState)
                                        _Logger.Log()(LogSeverityEnum.Trace, "Data receive: {0}-{1}", robotID, d);
                                    else
                                        _Logger.Log()(LogSeverityEnum.Info, "Data receive: {0}-{1}", robotID, d);

                                    if (ReceivedDataEvent != null)
                                        ReceivedDataEvent(new CEpsonRobotData(robotID, RobotIP, dataType, strContent, DateTime.Now));
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Fatal, ex.ToString());
            }
        }
    }
}
