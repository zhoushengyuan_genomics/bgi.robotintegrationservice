﻿using BGI.Common.Comm;
using BGI.Common.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BGI.RobotIntegrationService
{
    class CEpsonRobotRemoter
    {
        #region remote_control_protecol
        public static string RemoteCmdTerminator = Environment.NewLine;
        public enum EEpsonRobotRemoteCmd
        {
            Login = 0,
            Logout,
            Start,
            Stop,
            Pause,
            Continue,
            Reset,
            GetStatus
        }
        
        #endregion remote_control_protecol

        private string robotIP;
        private int port;
        private TCPClient tcpClientSender = new TCPClient();
        private Logger _Logger = LogMgr.GetLogger("CEpsonRobotClient");

        public CEpsonRobotRemoter(string robotIP,int port)
        {
            this.robotIP = robotIP;
            this.port = port;
            _Logger.SetMinLogLevel(LogSeverityEnum.Info);
        }

        public bool Connect()
        {
            return tcpClientSender.GetNewNetStreamIfNecessary(robotIP, port);   
        }

        public int Login()
        {
            return this.Send(EEpsonRobotRemoteCmd.Login);
        }

        public int Logout()
        {
            return this.Send(EEpsonRobotRemoteCmd.Logout);
        }

        public int Start()
        {
            return this.Send(EEpsonRobotRemoteCmd.Start,new string[] { "0"});
        }

        public int Stop()
        {
            return this.Send(EEpsonRobotRemoteCmd.Stop);
        }
        public int Pause()
        {
            return this.Send(EEpsonRobotRemoteCmd.Pause);
        }
        public int Continue()
        {
            return this.Send(EEpsonRobotRemoteCmd.Continue);
        }
        public int Reset()
        {
            return this.Send(EEpsonRobotRemoteCmd.Reset);
        }

        private int Send(EEpsonRobotRemoteCmd cmd,string[] cmdParam = null, int timeout = 3000)
        {
            int ret = -1;
            try
            {
                string strCmdParam = string.Empty;
                if (cmdParam != null) for (int i = 0; i < cmdParam.Length; i++) strCmdParam += string.Format("{0},", cmdParam[i]);
                strCmdParam = strCmdParam.TrimEnd(',');
                string strCmd = string.Format("${0}", cmd.ToString());
                if (strCmdParam != string.Empty) strCmd = string.Format("${0},{1}", cmd.ToString(), strCmdParam);

                string cmdResp = tcpClientSender.Send(robotIP, port, strCmd, timeout, RemoteCmdTerminator);
                ret = ParseCmdResp(cmd.ToString(), cmdResp);
                if (ret == 11 && cmd != EEpsonRobotRemoteCmd.Login)
                {
                    ret = Send(EEpsonRobotRemoteCmd.Login);
                    if (ret == 0) ret = ParseCmdResp(cmd.ToString(), tcpClientSender.Send(robotIP, port, strCmd, timeout, RemoteCmdTerminator));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ret;
        }
        private int ParseCmdResp(string cmd, string cmdResp)
        {
            int cmdCode = -1;
            try
            {
                if (cmdResp.IndexOf("#") == 0)
                {
                    cmdCode = 0;
                }
                else if (cmdResp.IndexOf("!") == 0)
                {
                    cmdCode = int.Parse(cmdResp.Substring(cmdResp.IndexOf(cmd) + cmd.Length + 1, 2));
                }
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Fatal, ex.ToString());
            }

            return cmdCode;
        }
    }
}
