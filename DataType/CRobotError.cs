﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BGI.RobotIntegrationService.DataType
{
    public class CRobotError
    {
        private string errorCode;
        private string errorDescrip;

        public CRobotError(string errorCode, string errorDescrip)
        {
            this.errorCode = errorCode;
            this.errorDescrip = errorDescrip;
        }

        public string ErrorCode
        {
            get
            {
                return errorCode;
            }
        }
        public string ErrorDescrip
        {
            get
            {
                return errorDescrip;
            }
        }
    }
}
