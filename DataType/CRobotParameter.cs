﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BGI.RobotIntegrationService.DataType
{
    public class CRobotParameter
    {
        private string name;
        private string value;
        public CRobotParameter(string name, string value)
        {
            this.name = name;
            this.value = value;
        }
        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        public string Value
        {
            get
            {
                return value;
            }

            set
            {
                this.value = value;
            }
        }
    }
}
