﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BGI.RobotIntegrationService.DataType
{
    public class CRobotPoint
    {
        private string name;
        private string j1;
        private string j2;
        private string j3;
        private string j4;
        private string j5;
        private string j6;
        private string j1Flag;
        private string j4Flag;
        private string j6Flag;
        private string hand;
        private string elbow;
        private string wrist;
        private string local;

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        public string J1
        {
            get
            {
                return j1;
            }

            set
            {
                j1 = value;
            }
        }

        public string J2
        {
            get
            {
                return j2;
            }

            set
            {
                j2 = value;
            }
        }

        public string J3
        {
            get
            {
                return j3;
            }

            set
            {
                j3 = value;
            }
        }

        public string J4
        {
            get
            {
                return j4;
            }

            set
            {
                j4 = value;
            }
        }

        public string J5
        {
            get
            {
                return j5;
            }

            set
            {
                j5 = value;
            }
        }

        public string J6
        {
            get
            {
                return j6;
            }

            set
            {
                j6 = value;
            }
        }

        public string J1Flag
        {
            get
            {
                return j1Flag;
            }

            set
            {
                j1Flag = value;
            }
        }

        public string J4Flag
        {
            get
            {
                return j4Flag;
            }

            set
            {
                j4Flag = value;
            }
        }

        public string J6Flag
        {
            get
            {
                return j6Flag;
            }

            set
            {
                j6Flag = value;
            }
        }

        public string Hand
        {
            get
            {
                return hand;
            }

            set
            {
                hand = value;
            }
        }

        public string Elbow
        {
            get
            {
                return elbow;
            }

            set
            {
                elbow = value;
            }
        }

        public string Wrist
        {
            get
            {
                return wrist;
            }

            set
            {
                wrist = value;
            }
        }

        public string Local
        {
            get
            {
                return local;
            }

            set
            {
                local = value;
            }
        }

        public CRobotPoint()
        {
        }

        public CRobotPoint(string name,
                            string j1,
                            string j2,
                            string j3,
                            string j4,
                            string j5,
                            string j6,
                            string j1Flag,
                            string j4Flag,
                            string j6Flag,
                            string hand,
                            string elbow,
                            string wrist,
                            string local)
        {
            this.Name = name;
            this.J1 = j1;
            this.J2 = j2;
            this.J3 = j3;
            this.J4 = j4;
            this.J5 = j5;
            this.J6 = j6;
            this.J1Flag = j1Flag;
            this.J4Flag = j4Flag;
            this.J6Flag = j6Flag;
            this.Hand = hand;
            this.Elbow = elbow;
            this.Wrist = wrist;
            this.Local = local;
        }
    }
}
