﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace BGI.RobotIntegrationService.EpsonRobot
{
    public partial class EpsonRobotConfig
    {
        private static EpsonRobotConfig instance;

        public static EpsonRobotConfig Instance
        {
            get
            {
                if (instance == null)
                    instance = getInstance("EpsonRobotConfig.xml");

                return instance;
            }
        }

        private static EpsonRobotConfig getInstance(string cfgFile)
        {
            EpsonRobotConfig instance = null;
            try
            {
                if (File.Exists(cfgFile))
                {
                    XmlDocument xml = new XmlDocument();
                    xml.Load(cfgFile);
                    string xmltxt = xml.OuterXml;
                    instance = EpsonRobotConfig.Parse(xmltxt);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            return instance;
        }

        public static EpsonRobotConfig Parse(string xmlStr)
        {
            using (StringReader sr = new StringReader(xmlStr))
            {
                XmlReader xr = new XmlTextReader(sr);
                return Parse(xr);
            }
        }

        private static EpsonRobotConfig Parse(XmlReader xr)
        {
            XmlSerializer xs = new XmlSerializer(typeof(EpsonRobotConfig));

            if (!xs.CanDeserialize(xr))
                return null;

            object tmp = xs.Deserialize(xr);
            EpsonRobotConfig result = tmp as EpsonRobotConfig;
            return result;
        }

    }

    public partial class EpsonRobotConfigLocal
    {
        public override string ToString()
        {
            return this.ID.ToString();
        }
    }

    public partial class EpsonRobotConfigMotionMode
    {
        public override string ToString()
        {
            return this.Name.ToString();
        }
    }

    public partial class EpsonRobotConfigCmd
    {
        public override string ToString()
        {
            return this.Name.ToString();
        }
    }
    public partial class EpsonRobotConfigCmdParameter
    {
        public override string ToString()
        {
            return string.Format("{0}={1}",this.Name,this.Value);
        }
    }
    
    public partial class IO
    {
        public override string ToString()
        {
            return this.Name.ToString();
        }
    }
    public partial class EpsonRobotConfigMIO
    {
        public override string ToString()
        {
            return this.Name.ToString();
        }
    }

    public partial class EpsonRobotConfigMotionParameter
    {
        [System.Xml.Serialization.XmlIgnore]
        public string VName
        {
            get
            {
                return Name;
            }

            set
            {
                Name = value;
            }
        }

        [System.Xml.Serialization.XmlIgnore]
        public string VValue
        {
            get
            {
                return Value;
            }

            set
            {
                Value = value;
            }
        }
        public string VDescription
        {
            get
            {
                return Description;
            }

            set
            {
                Description = value;
            }
        }        
    }

    public partial class WorkPos
    {
        [System.Xml.Serialization.XmlIgnore]
        public string VName
        {
            get
            {
                return Name;
            }

            set
            {
                Name = value;
            }
        }
        [System.Xml.Serialization.XmlIgnore]
        public float VX
        {
            get
            {
                return X;
            }

            set
            {
                X = value;
            }
        }
        [System.Xml.Serialization.XmlIgnore]
        public float VY
        {
            get
            {
                return Y;
            }

            set
            {
                Y = value;
            }
        }
        [System.Xml.Serialization.XmlIgnore]
        public float VZ
        {
            get
            {
                return Z;
            }

            set
            {
                Z = value;
            }
        }
        [System.Xml.Serialization.XmlIgnore]
        public float VU
        {
            get
            {
                return U;
            }

            set
            {
                U = value;
            }
        }
        [System.Xml.Serialization.XmlIgnore]
        public float VV
        {
            get
            {
                return V;
            }

            set
            {
                V = value;
            }
        }
        [System.Xml.Serialization.XmlIgnore]
        public float VW
        {
            get
            {
                return W;
            }

            set
            {
                W = value;
            }
        }
        [System.Xml.Serialization.XmlIgnore]
        public ushort VJ1Flag
        {
            get
            {
                return J1Flag;
            }

            set
            {
                J1Flag = value;
            }
        }
        [System.Xml.Serialization.XmlIgnore]
        public ushort VJ4Flag
        {
            get
            {
                return J4Flag;
            }

            set
            {
                J4Flag = value;
            }
        }
        [System.Xml.Serialization.XmlIgnore]
        public ushort VJ6Flag
        {
            get
            {
                return J6Flag;
            }

            set
            {
                J6Flag = value;
            }
        }
        [System.Xml.Serialization.XmlIgnore]
        public string VHand
        {
            get
            {
                return Hand;
            }

            set
            {
                Hand = value;
            }
        }
        [System.Xml.Serialization.XmlIgnore]
        public string VElbow
        {
            get
            {
                return Elbow;
            }

            set
            {
                Elbow = value;
            }
        }
        [System.Xml.Serialization.XmlIgnore]
        public string VWrist
        {
            get
            {
                return Wrist;
            }

            set
            {
                Wrist = value;
            }
        }
        [System.Xml.Serialization.XmlIgnore]
        public ushort VLocal
        {
            get
            {
                return Local;
            }

            set
            {
                Local = value;
            }
        }
        [System.Xml.Serialization.XmlIgnore]
        public ushort VID
        {
            get
            {
                return ID;
            }

            set
            {
                ID = value;
            }
        }
    }
}
