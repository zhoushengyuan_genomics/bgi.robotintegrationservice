﻿using BGI.Common.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BGI.RobotIntegrationService.PreciseRobot
{
    public class CCoordinate
    {
        private const string REPLY_PATTERN_FROM_CFG = @"^X:\s(\S+)\sY:\s(\S+)\sZ:\s(\S+)\sYaw:\s(\S+)\sPitch:\s(\S+)\sRoll:\s(\S+)(\r\n)?$";
        private const string REPLY_PATTERN_FROM_ROBOT = @"^(\S+)\s(\S+)\s(\S+)\s(\S+)\s(\S+)\s(\S+)\s(\S+)?(\r\n)?$";
        private const string REPLY_PATTERN_FROM_ROBOT2 = @"^0\s(\S+)\s(\S+)\s(\S+)\s(\S+)\s(\S+)\s(\S+)\s(\S+)?(\r\n)?$";
        private const float COORDINATE_FIND_THRESHOLD = 0.3f; // distance in mm robot can be from any one axis to find it.  TODO needs refining a bit.

        public float AxisX { get; set; }
        public float AxisY { get; set; }
        public float AxisZ { get; set; }
        public float Yaw { get; set; }
        public float Pitch { get; set; }
        public float Roll { get; set; }
        public float SafeBack { get; set; }

        public CCoordinate()
        {
        }

        public CCoordinate(string dataStr, bool fromRobot = false)
        {

            Regex r = new Regex(fromRobot ? REPLY_PATTERN_FROM_ROBOT : REPLY_PATTERN_FROM_CFG, RegexOptions.IgnoreCase);
            Match m = r.Match(dataStr);

            if (!m.Success)
            {
                r = new Regex(fromRobot ? REPLY_PATTERN_FROM_ROBOT2 : REPLY_PATTERN_FROM_CFG, RegexOptions.IgnoreCase);
                m = r.Match(dataStr);
                if (!m.Success)
                {
                    throw new BGIException(String.Format("Could not parse CCordinage string: {0}", dataStr));
                }
            }

            try
            {
                AxisX = float.Parse(m.Groups[1].Value);
                AxisY = float.Parse(m.Groups[2].Value);
                AxisZ = float.Parse(m.Groups[3].Value);
                Yaw = float.Parse(m.Groups[4].Value);
                Pitch = float.Parse(m.Groups[5].Value);
                Roll = float.Parse(m.Groups[6].Value);
            }
            catch (Exception)
            {
                throw new BGIException(String.Format("Could not parse CCordinage string: {0}", dataStr));
            }
        }

        public override string ToString()
        {
            return String.Format("X: {0} Y: {1} Z: {2} Yaw: {3} Pitch: {4} Roll: {5}",
                AxisX, AxisY, AxisZ, Yaw, Pitch, Roll);
        }

        public string MessageCoordinateString()
        {
            return String.Format("{0} {1} {2} {3} {4} {5}",
                AxisX, AxisY, AxisZ, Yaw, Pitch, Roll);
        }

        public override bool Equals(object obj)
        {
            CCoordinate ccIn = obj as CCoordinate;
            // If all points are within .1 then it is the same point.
            if (Math.Abs(ccIn.AxisX - AxisX) < COORDINATE_FIND_THRESHOLD
                && Math.Abs(ccIn.AxisY - AxisY) < COORDINATE_FIND_THRESHOLD
                && Math.Abs(ccIn.AxisZ - AxisZ) < COORDINATE_FIND_THRESHOLD
                && Math.Abs(ccIn.Yaw - Yaw) < COORDINATE_FIND_THRESHOLD
                && Math.Abs(ccIn.Pitch - Pitch) < COORDINATE_FIND_THRESHOLD
                && Math.Abs(ccIn.Roll - Roll) < COORDINATE_FIND_THRESHOLD)
            {
                return true;
            }
            return false;
        }

        public CCoordinate Clone()
        {
            CCoordinate retVal = new CCoordinate();

            retVal.AxisX = this.AxisX;
            retVal.AxisY = this.AxisY;
            retVal.AxisZ = this.AxisZ;
            retVal.Pitch = this.Pitch;
            retVal.Roll = this.Roll;
            retVal.Yaw = this.Yaw;
            retVal.SafeBack = this.SafeBack;

            return retVal;
        }

    }
}
