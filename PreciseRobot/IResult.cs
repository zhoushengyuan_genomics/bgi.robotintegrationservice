﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BGI.RobotIntegrationService.PreciseRobot
{
    public interface IResult
    {
        int ErrorCode { get; }
        string ErrorDesc { get; }
    }

    public class MResult: IResult
    {
        private int _ErrorCode;
        private string _ErrorDesc;
        public MResult(int errCode, string errDesc)
        {
            _ErrorCode = errCode;
            _ErrorDesc = errDesc;
        }

        public int ErrorCode
        {
            get
            {
                return _ErrorCode;
            }
        }

        public string ErrorDesc
        {
            get
            {
                return _ErrorDesc;
            }
        }
    }
}
