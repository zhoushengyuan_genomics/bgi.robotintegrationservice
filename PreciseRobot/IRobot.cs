﻿//-----------------------------------------------------------------------------
// Copyright 2016 (c) BGI.  All Rights Reserved.
// Confidential and proprietary works of BGI.
//-----------------------------------------------------------------------------
using BGI.RobotIntegrationService.Workflow;
using System.Collections.Generic;

namespace BGI.RobotIntegrationService.PreciseRobot
{
    public enum ERobotWorkflow
    {
        TestWF,
        TransferTipFromMicroserve2Hamilton1,
        TransferTipFromAmbistore2Hamilton1
    }

    /// <summary>
    /// Allowable robot axis
    /// </summary>
    public enum ERobotAxis
    {
        Axis1,
        Axis2,
        Axis3,
        Axis4,
        Axis5,
        Axis6,
        Unknow
    }

    public enum ERobotStatus
    {
        NotHomed,
        Idle,
        Busy,
        Error,
    }
    public enum RobotTypeEnum
    {
        VPF400Robot = 0,
        VRobotHitBot = 1,
		Staubli = 2,
		Epson = 3,
    }

    public enum ReplyCodeEnum
    {
        InvalidReply,
        Error,
        Aborted,
        Timeout,
        OK
    }

    /// <summary>
    /// Motion Profile enumberations to use for a single move.
    /// Robot specific code determines what sub-paramters to use, like exact speed, acceleration, accuracy, etc.
    /// </summary>
    public enum MotionProfileEnum
    {
        /// <summary>
        /// Slow movement with precise positioning enabled.
        /// </summary>
        Slow,
        /// <summary>
        /// Slow movement in a straight line.  Good for going in tight places.
        /// </summary>
        SlowStraight,
        /// <summary>
        /// Fast movement.
        /// </summary>
        Fast,
        /// <summary>
        /// Fast movement in a straight line.
        /// </summary>
        FastStraight
    }

    public enum ERobotTransferMaterial
    {
        TipsBox,
        Biorad,
        ReagentTrough300mL,
        DWPlate2_2mL,
        GreinerUVPlate,
        PCRPlate,
        AxygenDWPlate1_1mL
    }

    public enum EMotionMode
    {
        Cartesian,
        Joint
    }

    public interface IRobot
    {
        bool AutoRunReady { get; }
        /// <summary>
        /// Get/Set the percentage of maximum velocity.  This value applies to all axes.
        /// </summary>
        ushort PercentOfMaxVelocity { get; set; }
        ERobotStatus RobotStatus { get; }
        /// <summary>
        /// Returns the robot embedded software version.
        /// </summary>
        string SoftwareVersion { get; }
        /// <summary>
        /// Turns on or off the robot power.
        /// This may cause the gripper to drop anything being held if turned off while holding.
        /// </summary>
        void SetPower(bool isOn);
        /// <summary>
        /// Get all current coordinate axis
        /// </summary>
        /// <returns></returns>
        CCoordinate GetCurrentCCordinate();
        /// <summary>
        /// To get all angles of axis
        /// </summary>
        /// <returns></returns>
        JCoordinate GetCurrentJCoordinate();

        RobotReply GetProfile(ref Profile pf);
        void SetProfile(Profile pf);
        void LockAllAxis();
        void FreeAllAxis();
        void LockAxis(ERobotAxis axis);
        void FreeAxis(ERobotAxis axis);

        /// <summary>
        /// Blocking function to home robot
        /// </summary>
        int Home();
        /// <summary>
        /// Open the indicated gripper.
        /// </summary>
        int OpenGripper();
        /// <summary>
        /// Close the indicated gripper.
        /// </summary>
        /// <param name="gripperId">Gripper number</param>
        /// <returns>True if closed on a flowcell.  False if closed but no flowcell detected.</returns>
        int CloseGripper(ERobotTransferMaterial opMaterial);
        RobotReply MoveTo(JCoordinate targetPos);
        IResult ExcuteWorkFlow(string wfName);
        List<string> GetWorkflowList();
        RobotReply CheckLinkActive();
        RobotReply WaitForEOM();        
    }

    public interface IRobotReply
    {
        ReplyCodeEnum ReplyCode { get; set; }
        string ReplyCodeString { get; set; }
        string ReplyData { get; set; }
        int ReplyCodeInt { get; set; }
        int ReplyDataInt { get; }
    }
}
