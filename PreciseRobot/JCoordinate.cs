﻿using BGI.Common.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BGI.RobotIntegrationService.PreciseRobot
{
    public class JCoordinate
    {
        private const string REPLY_PATTERN_FROM_ROBOT = @"^(\S+)\s(\S+)\s(\S+)\s(\S+)\s?(\S+)?\s?(\S+)?$";

        public float Axis1 { get; set; } // Height
        public float Axis2 { get; set; } // Elbow 1
        public float Axis3 { get; set; } // Elbow 2
        public float Axis4 { get; set; } // Wrist

        public float Axis5 { get; set; } // Gripper 1

        public float Axis6 { get; set; } // Gripper 2
        public JCoordinate(float axis1, float axis2, float axis3, float axis4, float axis5, float axis6)
        {
            Axis1 = axis1;
            Axis2 = axis2;
            Axis3 = axis3;
            Axis4 = axis4;
            Axis5 = axis5;
            Axis6 = axis6;

        }
        public JCoordinate(string dataStr)
        {
            Regex r = new Regex(REPLY_PATTERN_FROM_ROBOT, RegexOptions.IgnoreCase);
            Match m = r.Match(dataStr);

            try
            {
                Axis1 = float.Parse(m.Groups[1].Value);
                Axis2 = float.Parse(m.Groups[2].Value);
                Axis3 = float.Parse(m.Groups[3].Value);
                Axis4 = float.Parse(m.Groups[4].Value);
                if (m.Groups.Count >= 6 && m.Groups[5].Value.Length > 0)
                {
                    Axis5 = float.Parse(m.Groups[5].Value);
                }
                if (m.Groups.Count == 7 && m.Groups[6].Value.Length > 0)
                {
                    Axis6 = float.Parse(m.Groups[6].Value);
                }
            }
            catch (Exception e)
            {
                throw new BGIException(String.Format("Could not parse joint angles reply from robot: {0}", dataStr));
            }
        }

        public override string ToString()
        {
            return String.Format("Axis1: {0} Axis2: {1} Axis3: {2} Axis4: {3} Axis5: {4} Axis6: {5}",
                Axis1, Axis2, Axis3, Axis4, Axis5, Axis6);
        }

        public string MessageJointString()
        {
            return String.Format("{0} {1} {2} {3} {4} {5}",
                Axis1, Axis2, Axis3, Axis4, Axis5, Axis6);
        }

        public float? GetAngleByAxis(int axis)
        {
            switch (axis)
            {
                case 1:
                    return Axis1;
                case 2:
                    return Axis2;
                case 3:
                    return Axis3;
                case 4:
                    return Axis4;
                case 5:
                    return Axis5;
                case 6:
                    return Axis6;
            }
            throw new ArgumentException(String.Format("Invalid axis number {0} used.", axis));
        }

        /// <summary>
        /// Check if angles 1 through 4 are equal.  Don't check grippers.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            JCoordinate JCIn = obj as JCoordinate;
            // If all points are within .1 then it is the same point.
            if (Math.Abs(JCIn.Axis1 - Axis1) < 0.2
                && Math.Abs(JCIn.Axis2 - Axis2) < 0.2
                && Math.Abs(JCIn.Axis3 - Axis3) < 0.2
                && Math.Abs(JCIn.Axis4 - Axis4) < 0.2
                && Math.Abs(JCIn.Axis5 - Axis5) < 0.2
                && Math.Abs(JCIn.Axis6 - Axis6) < 0.2)
            {
                return true;
            }
            return false;
        }
    }
}
