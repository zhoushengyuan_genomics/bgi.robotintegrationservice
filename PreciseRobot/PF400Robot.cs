﻿using BGI.Common.Comm;
using BGI.Common.Config;
using BGI.Common.Logging;
using BGI.RobotIntegrationService.Workflow;
using BGI.Common.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BGI.RobotIntegrationService.PreciseRobot
{
    public class PF400Robot : IRobot
    {
        private string robotIP;
        private int cmdPort;
        private float stepDist;  
        private static object _LockObj = new object();
        private TCPClient tcpClientSender = new TCPClient();
        private Logger _Logger = LogMgr.GetLogger("PF400Robot");
        public delegate void RobotStatusChangedHandler(ERobotStatus robotStatus,string msg);
        public event RobotStatusChangedHandler RobotStatusChanged;

        public PF400Robot()
        {
            ConfigMgr.Get.RegisterLogLevelUpdate(_Logger, "PF400Robot", "LogLevel");
        }

        private bool connected = false;
        public bool Connected
        {
            get
            {
                return connected;
            }
        }

        public bool AutoRunReady
        {
            get
            {
                bool ret = false;
                if (connected)
                {
                    // TBD  check robot setting
                    ret = IsStopAtSafePosition();
                }
                return ret;
            }
        }        

        public ushort PercentOfMaxVelocity
        {
            get
            {                
                string cmd = "mspeed";
                ushort percentMaxSpeed = ushort.MinValue;
                var reply = SendRobotCommand("mspeed");
                if (reply.ReplyCode != RobotReply.ReplyCodeEnum.OK)
                {
                    _Logger.Log()(LogSeverityEnum.Error, "Excute command[{0}] failed({1}).", cmd, reply);
                }
                else
                {
                    percentMaxSpeed = ushort.Parse(reply.ReplyData);
                }
                return percentMaxSpeed;
            }

            set
            {
                string cmd = string.Format("mspeed {0}", value > 100 ? 100 : value);
                var reply = SendRobotCommand(cmd);
                if (reply.ReplyCode != RobotReply.ReplyCodeEnum.OK)
                {
                    _Logger.Log()(LogSeverityEnum.Error, "Excute command[{0}] failed({1}).", cmd, reply);
                }
            }
        }

        public string SoftwareVersion
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public float StepDist
        {
            get
            {
                return stepDist;
            }
        }

        public bool IsSimulation
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public void ChangeStepDist(float dist)
        {
            stepDist = dist;
        }

        public int CloseGripper(ERobotTransferMaterial opMaterial)
        {
            throw new NotImplementedException();
        }

        private void ping(string server)
        {
            IPAddress ipAddr = IPAddress.Parse(server);
            Ping pingSender = new Ping();
            PingOptions options = new PingOptions();

            // Use the default Ttl value which is 128, 
            // but change the fragmentation behavior.
            options.DontFragment = true;

            // Create a buffer of 32 bytes of data to be transmitted. 
            string data = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
            byte[] buffer = Encoding.ASCII.GetBytes(data);
            int timeout = 120;

            _Logger.Log()(LogSeverityEnum.Info, "pinging {0}", server);

            PingReply reply = pingSender.Send(ipAddr, timeout, buffer, options);
            if (reply.Status == IPStatus.Success)
            {
                _Logger.Log()(LogSeverityEnum.Info, "Address: {0}", reply.Address.ToString());
                _Logger.Log()(LogSeverityEnum.Info, "RoundTrip time: {0}", reply.RoundtripTime);
                _Logger.Log()(LogSeverityEnum.Info, "Time to live: {0}", reply.Options.Ttl);
                _Logger.Log()(LogSeverityEnum.Info, "Don't fragment: {0}", reply.Options.DontFragment);
                _Logger.Log()(LogSeverityEnum.Info, "Buffer size: {0}", reply.Buffer.Length);
            }
            else
            {
                _Logger.Log(LogSeverityEnum.Info, "ping failed!");
                throw new Exception(string.Format("Cannot ping Robot controller PC ({0})", ipAddr));
            }
        }

        public void Connect(string ip, ushort port)
        {
            // Connect sequence
            // 1. Ping robot
            // 2. Set mode to 0 (only type of comms this suports)
            // 3. Set robot power on (high)
            // 4. Initialize the robot values.
            robotIP = ip;
            cmdPort = port;
            ping(robotIP);

            bool retVal;            
            retVal = tcpClientSender.GetNewNetStreamIfNecessary(robotIP, cmdPort);            
            if (retVal)
            {
                Init();
            }
            _Logger.Log(LogSeverityEnum.Info, string.Format("Connect to robot[{0}:{1}] {2}", robotIP, cmdPort, retVal ? "Success" : "Failed"));
        }

        public void DisConnect()
        {
            // Detach from robot
            string cmd = "attach 0";
            var reply = SendRobotCommand(cmd);
            if (reply.ReplyCode != RobotReply.ReplyCodeEnum.OK)
            {
                if (reply.ReplyCode == RobotReply.ReplyCodeEnum.Timeout)
                {
                    throw new Exception(string.Format("Robot did not reply to command[{0}]", cmd));
                }
                else
                {
                    throw new Exception(string.Format("Robot replied to command[{0}] with '{1}'", cmd, reply));
                }
            }

            RobotStatus = ERobotStatus.Error;
            connected = false;
        }

        public List<string> GetWorkflowList()
        {
            List<string> workflowList = new List<string>();

            foreach (var wf in WorkflowGroup.Instance.Workflow) workflowList.Add(wf.Name);

            return workflowList;
        }

        public IResult ExcuteWorkFlow(string wfName)
        {
            IResult ret = new MResult(0, "OK");            
            try
            {
                var workflow = WorkflowGroup.Instance.GetWorkflow(wfName);
                if (workflow == null) throw new Exception(string.Format("Unknow workflow[{0}]", wfName));

                RobotReply reply = CheckLinkActive();
                RobotStatus = ERobotStatus.Busy;
                for (int i = 0; i < workflow.Motion.Length; i++)
                {
                    var pos = workflow.Motion[i].Position;
                    reply = CheckLinkActive();
                    if (reply.ReplyCode != RobotReply.ReplyCodeEnum.OK) break;

                    reply = MoveTo(new JCoordinate(pos.J1, pos.J2, pos.J3, pos.J4, pos.J5, pos.J6));
                    if (reply.ReplyCode != RobotReply.ReplyCodeEnum.OK) break;

                    if (workflow.Motion[i].WaitForEom)
                    {
                        WaitForEOM();
                    }
                }
                ret = new MResult((int)reply.ReplyCode, reply.ToString());
            }
            catch (Exception ex)
            {
                ret = new MResult(-1, ex.Message);
                _Logger.Log()(LogSeverityEnum.Error, "{0}", ex);
            }
            finally
            {
                if (RobotStatus != ERobotStatus.Error) RobotStatus = ERobotStatus.Idle;
            }

            return ret;
        }

        public CCoordinate GetCurrentCCordinate()
        {
            throw new NotImplementedException();
        }

        public JCoordinate GetCurrentJCoordinate()
        {
            JCoordinate cord = null;
            RobotReply reply = SendRobotCommand("wherej");
            if (reply.ReplyCode == RobotReply.ReplyCodeEnum.OK)
            {
                cord = new JCoordinate(reply.ReplyData);
            }
            else
            {
                throw new BGIException("Could not get location.");
            }

            return cord;
        }

        private ERobotStatus robotStatus = ERobotStatus.NotHomed;
        public ERobotStatus RobotStatus
        {
            get
            {
                return robotStatus;
            }
            set
            {
                robotStatus = value;
                if (RobotStatusChanged != null)
                    RobotStatusChanged(robotStatus, string.Empty);
            }
        }

        public int Home()
        {
            throw new NotImplementedException();
        }

        public RobotReply MoveTo(JCoordinate targetPos)
        {
            return SendRobotCommand(string.Format("movej 1 {0}", targetPos.MessageJointString()));
        }

        public int OpenGripper()
        {
            throw new NotImplementedException();
        }

        public void SetPower(bool isOn)
        {
            if (RobotStatus != ERobotStatus.Busy)
            {
                RobotReply reply = SendRobotCommand(String.Format("hp {0} 30", isOn ? "1" : "0"));
                if (reply.ReplyCode == RobotReply.ReplyCodeEnum.OK)
                {
                    RobotStatus = isOn? ERobotStatus.Idle : ERobotStatus.Error;
                    Thread.Sleep(2000);
                }                
            }
        }

        public bool NegativeStepMove(ERobotAxis axisToMove, EMotionMode moveMode = EMotionMode.Joint)
        {
            return StepMove(axisToMove, (float)stepDist * -1);
        }

        public bool PositiveStepMove(ERobotAxis axisToMove, EMotionMode moveMode = EMotionMode.Joint)
        {
            return StepMove(axisToMove, (float)stepDist);
        }        

        public bool StepMove(ERobotAxis axisToMove, float distance, EMotionMode moveMode = EMotionMode.Joint)
        {
            bool ret = false;
            try
            {
                RobotStatus = ERobotStatus.Busy;
                JCoordinate newJ = GetCurrentJCoordinate();
                switch (axisToMove)
                {
                    case ERobotAxis.Axis1:
                        newJ.Axis1 += distance;
                        break;
                    case ERobotAxis.Axis2:
                        newJ.Axis2 += distance;
                        break;
                    case ERobotAxis.Axis3:
                        newJ.Axis3 += distance;
                        break;
                    case ERobotAxis.Axis4:
                        newJ.Axis4 += distance;
                        break;
                    case ERobotAxis.Axis5:
                        newJ.Axis5 += distance;
                        break;
                    case ERobotAxis.Axis6:
                        newJ.Axis6 += distance;
                        break;
                }

                var reply = MoveTo(newJ);
                if (reply.ReplyCode != RobotReply.ReplyCodeEnum.OK)
                    return false;

                ret = WaitMotionExcuteResult(newJ);
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Error, "{0}", ex);
            }
            finally
            {
                RobotStatus = ret ? ERobotStatus.Idle: ERobotStatus.Error;
            }

            return ret;
        }

        #region Send Command

        /// <summary>
        /// Entry point to send commands to the controler (as opposed to the robot).
        /// Cannot do any move commands here, only controler level stuff.
        /// </summary>
        //private RobotReply SendControlerCommand(string cmd)
        //{
        //    return SendCommand(ControllerPort, cmd);
        //}

        /// <summary>
        /// Entry point to send commands to the robot (as opposed to the controler).
        /// </summary>
        /// <param name="cmd">string command to send.</param>
        /// <returns>Data returned.</returns>
        private RobotReply SendRobotCommand(string cmd)
        {
            return SendCommand(cmdPort, cmd);
        }

        /// <summary>
        /// Entry point to send commands to the robot or controler port.
        /// </summary>
        private RobotReply SendCommand(int port, string cmd)
        {
            lock (_LockObj)
            {
                string cmdReply = string.Empty;
                RobotReply reply = null;
                int timeoutSec = 30;                        // TODO config
                _Logger.Log()(LogSeverityEnum.Info, "SendCommand: '{0}'", cmd);
                try
                {
                    cmdReply = tcpClientSender.Send(robotIP, port, cmd, timeoutSec);
                }
                catch (Exception ex)
                {
                    RobotStatus = ERobotStatus.Error;
                    _Logger.Log()(LogSeverityEnum.Error, "Exception occurred sending motion command '{0}': {1}", cmd, ex.ToString());
                    throw;
                }
                _Logger.Log()(LogSeverityEnum.Info, "Command[{0}] respone: {1}", cmd, cmdReply);

                reply = new RobotReply(cmdReply);
                if (reply.ReplyCode != RobotReply.ReplyCodeEnum.OK)
                {
                    RobotStatus = ERobotStatus.Error;
                    _Logger.Log()(LogSeverityEnum.Error, "Command '{0}' failed to reply ({1})", cmd, reply);
                }

                return reply;
            }
        }

        #endregion

        private bool WaitMotionExcuteResult(JCoordinate destPos, ushort timeout = 60)
        {
            bool ret = false;
            try
            {
                ushort times = 0;
                do
                {
                    if (CheckLinkActive().ReplyCode == RobotReply.ReplyCodeEnum.OK)
                    {
                        JCoordinate crtPos = GetCurrentJCoordinate();
                        if (destPos.Equals(crtPos))
                            return true;
                    }                    

                    times++;
                    Thread.Sleep(1000);                    
                } while (times < timeout);

            }
            catch(Exception ex)
            {

            }

            return ret;
        }

        public void Init()
        {
            try
            {
                // Now have a connection to the robot.  Start initializing it.

                // Set to computer responses, not verbose
                RobotReply reply = SendRobotCommand("mode 0");
                if (reply.ReplyCode != RobotReply.ReplyCodeEnum.OK)
                {
                    throw new Exception(string.Format("mode command failed."));
                }

                // Kind of redundant.  Checking if we can talk to controler.  Already know that, but harmless.
                reply = SendRobotCommand("nop");
                if (reply.ReplyCode != RobotReply.ReplyCodeEnum.OK)
                {
                    if (reply.ReplyCode == RobotReply.ReplyCodeEnum.Timeout)
                    {
                        throw new Exception(string.Format("Robot did not reply to NOP command"));
                    }
                    else
                    {
                        throw new Exception(string.Format("Robot replied to NOP command with '{0}'", reply));
                    }
                }

                // Must turn on power on connect so we can attach to the robot
                SetPower(true);

                // Attach to the robot

                // Have to run this in a loop just in case another process (such as the Auto Home feature) is attached.

                int attachAttempts = 0;
                for (; attachAttempts < 60; attachAttempts++)
                {
                    reply = SendRobotCommand("attach 1");
                    if (reply.ReplyCode != RobotReply.ReplyCodeEnum.OK)
                    {
                        if (reply.ReplyCode == RobotReply.ReplyCodeEnum.Timeout)
                        {
                            throw new Exception(string.Format("Robot did not reply to Attach command"));
                        }
                        else
                        {
                            if (reply.ReplyCodeInt == -1006)
                            {
                                _Logger.Log()(LogSeverityEnum.Warning, "Failed to attach to robot because another process already attached ({0}).  Retrying after sleep 1 sec.", attachAttempts);
                                Thread.Sleep(1000);
                            }
                            else
                            {
                                throw new Exception(string.Format("Robot replied to Attach command with '{0}'", reply));
                            }
                        }
                    }
                    else
                    {
                        break;
                    }
                }

                if (attachAttempts >= 60)
                {
                    throw new Exception(string.Format("Robot replied to Attach to robot.  See log for details."));
                }

                // Home robot if needed.
                reply = SendRobotCommand("pd 2800"); // Get current Homed status from robot DB
                if (reply.ReplyCode == RobotReply.ReplyCodeEnum.OK)
                {
                    if (reply.ReplyDataInt == 0)
                    {
                        _Logger.Log()(LogSeverityEnum.Warning, "Robot found to not be Homed.  Homing now.");
                        reply = SendRobotCommand("home");
                        _Logger.Log()(LogSeverityEnum.Warning, "Robot Homing completed.");
                        if (reply.ReplyCode != RobotReply.ReplyCodeEnum.OK)
                        {
                            throw new Exception(string.Format("Robot home failed.  See log for details."));
                        }
                    }
                }

                // Enable/Lock all Axis
                reply = SendRobotCommand("FreeMode -1");
                if (reply.ReplyCode != RobotReply.ReplyCodeEnum.OK)
                {
                    if (reply.ReplyCode == RobotReply.ReplyCodeEnum.Timeout)
                    {
                        throw new Exception(string.Format("Robot did not reply to FreeMode command"));
                    }
                    else
                    {
                        throw new Exception(string.Format("Robot replied to FreeMode command with '{0}'", reply));
                    }
                }

                // Reset the threads for a robot.  Must go over the status thread, not controller or robot threads.
                //_Logger.Log(LogSeverityEnum.Info, "Sending reset command to robot 1.");
                //RobotReply reply = SendControlerCommand("reset 1");

                // Attach the robot before we can talk to it
                reply = SendRobotCommand("attach 1");
                if (reply.ReplyCode != RobotReply.ReplyCodeEnum.OK)
                {
                    if (reply.ReplyCode == RobotReply.ReplyCodeEnum.Timeout)
                    {
                        throw new Exception(string.Format("Robot did not reply to Attach"));
                    }
                    else
                    {
                        throw new Exception(string.Format("Robot replied to Profile command with '{0}'", reply));
                    }
                }
                RobotStatus = ERobotStatus.Idle;
                connected = true;
            }
            catch(Exception ex)
            {
                connected = false;
                throw ex;
            }
        }

        public void LockAllAxis()
        {
            _Logger.Log()(LogSeverityEnum.Debug, "Locking all Axis.");
            RobotReply reply = SendRobotCommand("FreeMode -1");
        }

        public void FreeAllAxis()
        {
            _Logger.Log()(LogSeverityEnum.Debug, "Free all Axis.");
            RobotReply reply = SendRobotCommand("FreeMode -0");
        }

        public void LockAxis(ERobotAxis axis)
        {
            RobotReply reply = SendRobotCommand(string.Format("setBrake -{0}", (ushort)axis));
        }

        public void FreeAxis(ERobotAxis axis)
        {
            RobotReply reply = SendRobotCommand(string.Format("releaseBrake -{0}", (ushort)axis));            
        }

        public void SetProfile(Profile pf)
        {
            RobotReply reply = SendRobotCommand(string.Format("profile {0}", pf.ProfileCommandString));
        }

        public RobotReply GetProfile(ref Profile pf)
        {
            RobotReply reply = SendRobotCommand(string.Format("profile {0}", pf.Idx));
            if (reply.ReplyCode == RobotReply.ReplyCodeEnum.OK)
            {
                Console.WriteLine(reply.ReplyData);
                pf = Profile.ParseProfile(reply.ReplyData);
            }

            return reply;
        }

        public bool IsStopAtSafePosition()
        {
            bool ret = false;
            try
            {
                var safePos = WorkflowGroup.Instance.GetPosition("SafePosition");
                var crtPos = GetCurrentJCoordinate();
                if (safePos != null && crtPos != null)
                {
                    if (Math.Abs(safePos.J1 - crtPos.Axis1) < 1.0
                        && Math.Abs(safePos.J2 - crtPos.Axis2) < 1.0
                        && Math.Abs(safePos.J3 - crtPos.Axis3) < 1.0
                        && Math.Abs(safePos.J4 - crtPos.Axis4) < 1.0
                        && Math.Abs(safePos.J5 - crtPos.Axis5) < 1.0
                        && Math.Abs(safePos.J6 - crtPos.Axis6) < 1.0)
                    {
                        ret = true;
                    }
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }

            return ret;
        }

        public RobotReply CheckLinkActive()
        {
            return SendRobotCommand("nop");
        }

        public RobotReply WaitForEOM()
        {
            return SendRobotCommand("waitForEom");
        }
    }
}
