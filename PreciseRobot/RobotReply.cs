﻿using BGI.Common.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BGI.RobotIntegrationService.PreciseRobot
{
    public class RobotReply
    {
        // Version of TCP command server this library was created from.  Taken from Globals.
        public readonly string VersionString = "";
        public const string ABORTED_REPLY = "ClientAborted";
        public const string TIMEOUT_REPLY = "CommandTimeout";
        public const string ERROR_REPLY = "ErrorDuringCommand";

        public enum ReplyCodeEnum
        {
            InvalidReply,
            Error,
            Aborted,
            Timeout,
            OK
        }

        private const string REPLY_PATTERN = @"^(-?\d+)\s*(.*)\r\n$";

        public ReplyCodeEnum ReplyCode { get; set; }
        public string ReplyCodeString { get; set; }
        public string ReplyData { get; set; }
        public int ReplyCodeInt { get; set; }

        public RobotReply(string replyStr)
        {
            // First check for timeout and abort strings.  They don't follow the PA reply structure.
            if (String.Equals(replyStr, ABORTED_REPLY, StringComparison.CurrentCultureIgnoreCase))
            {
                ReplyCode = ReplyCodeEnum.Aborted;
                ReplyData = "";
            }
            else if (String.Equals(replyStr, TIMEOUT_REPLY, StringComparison.CurrentCultureIgnoreCase))
            {
                ReplyCode = ReplyCodeEnum.Timeout;
                ReplyData = "";
            }
            else if (String.Equals(replyStr, ERROR_REPLY, StringComparison.CurrentCultureIgnoreCase))
            {
                ReplyCode = ReplyCodeEnum.Aborted;
                ReplyData = "";
            }
            else
            {
                // OK, so this is a real reply from robot.  Now parse it.

                Regex r = new Regex(REPLY_PATTERN, RegexOptions.IgnoreCase);
                Match m = r.Match(replyStr);

                if (m.Success)
                {

                    string replyCodeStr = m.Groups[1].Value;
                    ReplyData = m.Groups[2].Value;

                    // Get the reply code (first number) and use to get success or error.
                    int replyCodeInt = 0;
                    if (!int.TryParse(replyCodeStr, out replyCodeInt))
                    {
                        throw new BGIException("Could not parse error code from _Robot to integer.");
                    }
                    ReplyCodeEnum tmpCE = ReplyCodeEnum.InvalidReply;
                    string tmpErrStr = "";
                    ReplyCodeInt = replyCodeInt;
                    ParseCode(replyCodeInt, out tmpCE, out tmpErrStr);
                    ReplyCode = tmpCE;
                    ReplyCodeString = tmpErrStr;
                }
                else
                {
                    throw new BGIException("Could not match string '{0}' with pattern '{1}.", replyStr, REPLY_PATTERN);
                }
            }
        }

        public int ReplyDataInt
        {
            get
            {
                int intVal = 0;
                if (int.TryParse(ReplyData, out intVal))
                {
                    return intVal;
                }
                throw new ArgumentException(String.Format("ReplyData ('{0}') could not convert to Integer", ReplyData));
            }
        }

        public override string ToString()
        {
            return String.Format("{0}:{1} '{2}'", ReplyCode, ReplyCodeString, ReplyData);
        }

        #region Private Methods and Properties

        private void ParseCode(int code, out ReplyCodeEnum codeE, out string codeStr)
        {
            switch (code)
            {
                case 0:
                    codeE = ReplyCodeEnum.OK;
                    codeStr = "";
                    break;
                case -1012:
                    codeE = ReplyCodeEnum.InvalidReply;
                    codeStr = "-1012 Joint out-of-range";
                    break;
                case -2800:
                    codeE = ReplyCodeEnum.InvalidReply;
                    codeStr = "-2800 *Warning Parameter Mismatch*";
                    break;
                case -2801:
                    codeE = ReplyCodeEnum.InvalidReply;
                    codeStr = "-2801 *Warning No Parameters";
                    break;
                case -2802:
                    codeE = ReplyCodeEnum.InvalidReply;
                    codeStr = "-2802 *Warning Illegal move command*";
                    break;
                case -2803:
                    codeE = ReplyCodeEnum.InvalidReply;
                    codeStr = "-2803 *Warning Invalid joint angles*";
                    break;
                case -2804:
                    codeE = ReplyCodeEnum.InvalidReply;
                    codeStr = "-2804 *Warning: Invalid Cartesian coordinate values*";
                    break;
                case -2805:
                    codeE = ReplyCodeEnum.InvalidReply;
                    codeStr = "-2805 *Unknown command*";
                    break;
                case -2806:
                    codeE = ReplyCodeEnum.InvalidReply;
                    codeStr = "-2806 *Command Exception*";
                    break;
                case -2807:
                    codeE = ReplyCodeEnum.InvalidReply;
                    codeStr = "-2807 *Warning cannot set Input states*";
                    break;
                case -2808:
                    codeE = ReplyCodeEnum.InvalidReply;
                    codeStr = "-2808 *Not allowed by this thread*";
                    break;
                case -2809:
                    codeE = ReplyCodeEnum.InvalidReply;
                    codeStr = "-2809 *Invalid _Robot type*";
                    break;
                case -2810:
                    codeE = ReplyCodeEnum.InvalidReply;
                    codeStr = "-2810 *Invalid serial command*";
                    break;
                case -2811:
                    codeE = ReplyCodeEnum.InvalidReply;
                    codeStr = "-2811 *Invalid _Robot number*";
                    break;
                case -2812:
                    codeE = ReplyCodeEnum.InvalidReply;
                    codeStr = "-2812 *Robot already selected*";
                    break;
                case -1600:
                    codeE = ReplyCodeEnum.InvalidReply;
                    codeStr = "-1600 Robot Power Off";
                    break;
                case -1046:
                    codeE = ReplyCodeEnum.InvalidReply;
                    codeStr = "-1026 Robot Power Off";
                    break;
                case -1040:
                    codeE = ReplyCodeEnum.Error;
                    codeStr = "-1040 Position too far";
                    break;
                case -1042:
                    codeE = ReplyCodeEnum.Error;
                    codeStr = "-1042 Can't change robot config";
                    break;
                case -1006:
                    codeE = ReplyCodeEnum.Error;
                    codeStr = "-1006 Robot already attached";
                    break;
                case -1009:
                    codeE = ReplyCodeEnum.Error;
                    codeStr = "-1009 No robot attached";
                    break;
                case -1043:
                    codeE = ReplyCodeEnum.Error;
                    codeStr = "-1043 *Asynchronous soft error*";
                    break;
                case -3100:
                    codeE = ReplyCodeEnum.Error;
                    codeStr = "-3100, *Hard envelope error*";
                    break;
                case -3122:
                    codeE = ReplyCodeEnum.Error;
                    codeStr = "-3122, *Soft envelope error*";
                    break;
                case -3147:
                    codeE = ReplyCodeEnum.Error;
                    codeStr = "-3147, *Abnormal envelope error*";
                    break;
                default:
                    codeE = ReplyCodeEnum.Error;
                    codeStr = String.Format("{0}, Unsupported error code", code);
                    break;
            }
        }


        /*
         * -1000  Invalid robot number A robot number has been specified that is less than 1 or more than the number of configured robots.  
        -1001  Undefined robot A robot number must be specified (1 to N), but no number was defined.  For example, this error can occur if you are referencing a Parameter Database value that requires that a robot be specified as the "unit" (second) parameter but this value is left blank.  
        -1002  Invalid axis number An axis number has been specified that is less than 1 or more than the number of axes configured in the referenced robot.  For example, this error will be generated if you are accessing a location's angle value (location.angle(n)) but the axis number is undefined or set to 0.  
        -1003  Undefined axis An axis has been specified that is not configured for the referenced robot.  This can occur if an axis bit mask has been specified that references an axis that is not currently configured.  
        -1004  Invalid motor number A motor number has been specified that is less than 1 or more than the number of motors configured in the referenced robot. 
        -1005  Undefined motor A motor has been specified that is not configured for the referenced robot.  This can occur if a motor bit mask has been specified that references a motor that is not currently configured.  
        -1006  Robot already attached An Auto Execution task or a GPL project has attempted to gain control of a robot by executing a Robot.Attach method or similar function, but the specified robot is already in use by another task.  Alternately, this error is generated if you attempt to Attach or Select a robot, but the task is already attached to a different robot.  
        -1007  Robot not ready to be attached An Auto Execution task or a GPL project has attempted to gain control of a robot by executing a Robot.Attach method or similar function, but the specified robot is not in a state where it can be attached.  If this was generated by a GPL project, it might indicate:

           1. That the system is configured to execute DIOMotion blocks instead of a GPL motion program (DataID 200) or "Auto start auto execute mode" (DataID 202) is not set to TRUE. If so, please check the Setup>Startup Configuration web page to verify the current setup.

        -1008  Can't detached a moving robot An operation is attempting to Detach a robot that is currently moving.  Normally, it is not possible to generate this error condition because the Robot.Attached method automatically waits for the robot to stop before attempting the detach operation.  
        -1009  No robot attached A function or method was executed that required that a robot be ATTACHED.  This error is often generated if you attempt to execute one of the methods in the Move Class without first attaching the robot.  Correct your GPL program by inserting a Robot.Attached method prior to executing the instruction that contains the Move Class method.  
        -1010  No robot selected A function or method was executed that required that the robot be SELECTED.  By default, the first robot or the Attached robot is set as the selected robot.  A number of "readonly" operations require that a robot be selected, e.g. location.Here.  Correct your GPL program by inserting a Robot.Selected method prior to executing the instruction that generated the exception. 

        -1011  Illegal during special Cartesian mode The robot is performing a special Cartesian trajectory mode such as conveyor tracking, outputting a DAC signal based upon the Cartesian tool tip speed,  performing real-time trajectory modification, etc.  When the trajectory generator is in this mode, the requested operation that produced this error is not permitted to execute.  For example, jogging or moving along a joint interpolated trajectory or changing the tool length cannot be performed while tracking a conveyor belt.  You must either terminate the current Cartesian trajectory mode with a Robot.RapidDecel or other means before initiating the new robot control mode or you must modify your program to use a method consistent with the current trajectory mode.  For example, if you attempted to initiate a joint interpolated motion, use a Cartesian straight-line motion instead.  
        -1012  Joint out-of-range This indicates that the specified robot axes are either beyond or were attempted to be moved beyond their software limits, i.e. outside of their permitted ranges of travel. 

        This error is also generated if you attempt to set the minimum and maximum soft and hard joint limits to inconsistent values.   If you are narrowing the limits, you should set the new soft limits first and then change the hard limits.  If you attempt to make both changes at the same time with the web interface, the system will process the new hard limits first, determine that they violate the old soft limits, and generate this error message. 

        -1013  Motor out-of-range This indicates that the specified robot motors are either beyond or were attempted to be moved beyond their software limits.  This error is also generated if you attempt to set the minimum and maximum soft and hard motor limits to inconsistent values. For most robots, this error code will never be generated because the joint limits will be used exclusively.  However, some robot's have coupled motors such that it may be possible to not violate a joint limit and still encounter the extreme travel limit of a motor. In these cases, this error message may be generated even when it appears that the robot's joint's are within their permitted ranges of travel.  
        -1014  Time out during nulling At the end of a program generated motion, if the axes of the robot take too long to achieve the "InRange" constraint limits, this error message will be generated and program execution will be terminated.  This may indicate that the InRange limit has been set too tightly or that the robot may not be able to get to the specified final position due to an obstruction.  
        -1015  Invalid roll over spec The Continuous Turn (encoder roll-over compensation) angle (DataID 2302) was set non-zero for an axis and the axis is not designed to support continuous turning.  Please review the information for your kinematic module in the Kinematic Library documentation and ensure that the axis has been designed to support this feature. 

        -1016  Torque control mode incorrect Either the system is in torque control mode and should not be for the currently execution instruction or the system should be in torque control mode but is not.   This can be generated in the following situations: 

           1. Torque control mode is active and an instruction is executed to start External Trajectory mode, Jog Control mode, or torque control mode. 
           2. An instruction is issued to set the torque values, but no motors are in torque control mode. 

        -1017  Not in position control mode A motion control instruction was initiated that requires that the robot be in the standard position controlled mode and the robot is in a special control mode, e.g. velocity control or jogging mode.  For example, to initiate any of the following, the robot must be in the standard position control mode:  torque control mode, velocity control mode, jog mode or any of the Move Class position controlled methods (e.g. Move.Loc).  
        -1018  Not in velocity control mode This error is generated if an instruction attempts to set the velocity mode speeds of an axis, but the system is not in velocity control mode.  
        -1019  Timeout sending servo setpoint The GPL trajectory generator sends a setpoint to the servos at the time interval determined by DataID 600, (Trajectory Generator update period in sec). This error occurs if a new setpoint is ready but the previous setpoint has not been sent. Verify that the DataID 603 (Servo update period in sec) value is one-half or less than the value of DataID 600 (Trajectory Generator update period in sec).

        In a servo network system, this error may indicate a network failure or unexpected network congestion. Provided that DataID 600 and 603 are set properly, this error should never be seen in a non-servo-network system. If the problem persists, contact Precise Automation. 
        -1020  Timeout reading servo status The servos send status information to GPL at the time interval determined by DataID 600, (Trajectory Generator update period in sec). This error occurs if no status information has been received by GPL for the past 32 milliseconds.

        If this error occurs when you are configuring a new controller system, it might indicate that you have changed some system parameters that are marked as "Restart required".  However, you have attempted to operate the system without rebooting the controller.  This can be corrected by restarting the controller. 

        In a servo network system, this error may indicate a network failure or unexpected network congestion.  This error should never be seen in a properly configured non-servo-network system.  If the problem persists, contact Precise Automation.  
        -1021  Robot not homed An operation was invoked that requires that the robot's motors be homed.  If a robot is equipped with incremental encoders, when the controller is restarted, the system does not have any knowledge of where each axes is located in the workspace.  Homing establishes a repeatable "zero" position for each axis. The robot can be homed by pressing a button on the web Operator Control Panel, the web Virtual Manual Control Panel or via a program instruction. 
        -1022  Invalid homing parameter While executing the homing sequence an invalid parameter was encountered.  This indicates that one of the Parameter Database values that controls homing (DataID 28xx) has been incorrect set.  For example, an illegal homing method may be specified (DataID 2803) or the homing speed may be zero (DataID 2804). 
        -1023  Missed signal during homing During the homing operation with the robot moving, an error was detected prior to finding the signal that was expected.  The detected error is most likely caused by an unexpected hardstop encountered or a over-travel limit switch tripping. 
        -1024  Encoder index disabled This is normally generated by the homing routines.  If a selected homing method tests for an encoder zero index signal and the encoder index is not enabled, this error is generated.  This typically occurs if the "Encoder counts used for resolution calc" (DataID 10203) or the "Encoder revs used for resolution" (DataID 10204) are not properly setup.  
        -1025  Timeout enabling power A request to enable robot power has failed to complete within the timeout period. Either a hardware failure has prevented power from coming on, or the timeout period is too short. Check the System Messages on the web interface Operator Control Panel for additional errors that may indicate a hardware failure. Verify that the controller is properly cabled. Try increasing the value of parameter "Timeout waiting for power to come on in sec" (DataID 262). This error may also occur as a GPL exception if power does not come on when the Controller.PowerEnabled method is used with a non-zero timeout parameter.  
        -1026  Timeout enabling amp Robot power has been turned off during the robot power-on sequence because one or more power amplifiers have not become ready within the timeout period.   Please try the following procedures:

           1. Check the System Messages on the web interface Operator Control Panel for additional errors that may indicate a hardware failure. 
           2. Verify that the controller is properly cabled. 
           3. Try increasing the value of parameter "Timeout waiting for amps to come on in sec" (DataID 264).
           4. If this occurs when the controller is first booted, try repeating the enable power operation after delaying for 1 minute.  This error message can be generated if the robot includes absolute encoders that take a minute or so to complete their initialize before becoming ready to operate. 
        -1027  Timeout starting commutation Robot power has been turned off during the robot power-on sequence because one or more motors have not completed their commutation sequence within the timeout period. Check the System Messages on the web interface Operator Control Panel for additional errors that may indicate a hardware failure. Try increasing the value of parameter "Timeout waiting for commutation in sec" (DataID 266). Verify that the controller is properly cabled and that the encoders and motors are wired correctly. See documentation section First Time Mechanism Integration and verify that the controller commutation parameters are set correctly for your motor and encoder combination. Verify that the FPGA firmware on the controller supports your motor and encoder combination.  
        -1028  Hard E-STOP A hard E-Stop condition has been detected.  Any robot motion in progress is stopped rapidly and robot power is turned off.  One the following has occurred:

           1. A front panel E-Stop loop ("ESTOP_L 1" or "ESTOP_L 2") has been broken.
           2. The digital input signal specified by "Hard E-Stop DIN" (DataID 244) has been asserted.
           3. The parameter database item "Hard E-Stop" (DataID 243) has been set to TRUE. 
        -1029  Asynchronous error An error signal from the servos or trajectory generator has been received by GPL, but no specific error code has been received.  The error log entry immediately following this one normally indicates the actual error.

        Error signaling within the motion subsystem is a two-step process.  When an error is first detected, a signal is sent to GPL immediately so that a controlled deceleration sequence can begin.  This first signal generates a -1029 error code.  Several milliseconds later, a more specific error code is sent to identify the source of the error.  This second error code overwrites the -1029 error.  Error -1029 is only seen if the error log is sampled after the initial error signal is received and before the specific error code is received. 
        -1030  Fatal asynchronous error A severe error signal from the servos or trajectory generator has been received by GPL, but no specific error code has been received.  The error log entry immediately following this one normally indicates the actual error.

        Error signaling within the motion subsystem is a two-step process.  When a severe error is first detected, a signal is sent to GPL immediately so that a controlled deceleration sequence can begin.  This first signal generates a -1030 error code.  Several milliseconds later, a more specific error code is sent to identify the source of the error.  This second error code overwrites the -1030 error.  Error -1030 is only seen if the error log is sampled after the initial error signal is received and before the specific error code is received.

        Unlike standard errors, a severe error prevents robot power from being enabled until the controller is rebooted or "Reset fatal error" (DataID 247) is set to 1.

        -1031  Analog input value too small When reading an analog input signal, if after the scale and offset is applied, the value of the signal is lower than the limit set by "Gen AIO In min scaled value" (DataID 526), the analog value is set to the minimum value and this error is generated.  
        -1032  Analog input value too big When reading an analog input signal, if after the scale and offset is applied, the value of the signal is higher than the limit set by "Gen AIO In max scaled value" (DataID 527), the analog value is set to the maximum value and this error is generated.  
        -1033  Invalid Cartesian value For robots with less than 6 independent degrees-of-freedom, certain combinations of tool orientations and positions are not possible.  This does not indicate an axis limit stop error such as when a program attempts to move a linear axis beyond its end of travel. This error refers to positions and orientations that are not possible even if each axis of the robot has an unlimited range of motion.  For example, for a 4-axis Cartesian robot with a theta axis, the tool cannot be rotated about the world X or Y axis.  So if an instruction has a destination that requires that the tool be moved from pointing down to pointing up, this error will be generated.  
        -1034  Negative overtravel This is generated when the optional negative travel limit hardware switch has been tripped.  This error indicates that a specified axis it outside of its permitted range-of-motion. 
        -1035  Positive overtravel This is generated when the optional positive travel limit hardware switch has been tripped.  This error indicates that a specified axis it outside of its permitted range-of-motion. 
        -1036  Kinematics not installed An operation was invoked that requires that the system be able to convert between joint and Cartesian (XYZ) coordinates.  However, the system software has not been configured with a robot kinematics (geometry) module.  The kinematic modules are selected using the "Robot types" (DataID 116) and are described in the Kinematics Library section of the Documentation Library.  Select an appropriate module and restart the system.  If there is not an appropriate kinematics module, contact Precise Automation.  
        -1037  Motors not commutated The operation that was attempted may not require that the robot's motors be homed, however, they must have their commutation reference established.  Setting the commutation reference provides the controller with the knowledge of how to energize the individual motor phases as the motor rotates.  For example, motors can be placed into torque control mode after they have been commutated and without homing the motors.  
        -1038 Project generated robot error This error is never generated automatically and is provided solely as a convenience for GPL application programs.  GPL projects can use the Throw instruction to generate this special error to indicate special application errors that are robot specific. 
        -1039 Position too close  This indicates that an XYZ destination is too close to the center of the robot and cannot be reached.  For example, your robot may have an inner and an outer rotary link that dictates the radial distance of the gripper.  If the outer link is shorter than the inner link, there will be a circular region at the center of the robot that cannot be accessed.  In other cases, if the inner and outer link are the same lengths, the robot may be able to reach the center position, but such a position might be a mathematical singularity where two or more joints degenerate into the same motion. These types of conditions are signaled by this error code. 
        -1040 Position too far  This indicates that an XYZ destination is beyond the robot's reach.  This is typically generated by robot's with rotary links where the position is beyond the range of the fully outstretched links.  To avoid excessive joint rotation speeds, this error may be signaled a few degrees before the fully extended position in manual jog mode or when the robot is moving along a straight-line path.  
        -1041 Invalid Base transform  The Base transformation for a robot is required to have a zero pitch value.  That is, a Base transform can translate the robot in all three directions and can rotate the robot about the world Z-axis, but it can not rotate the robot about the world X-axis or Y-axis.  
        -1042 Can't change robot config A motion was initiated that attempted to change the robot configuration (e.g. Righty vs. Lefty) when such a change is not permitted.  For example, you cannot change the robot configuration during a Cartesian straight-line motion.  Normally, if you specify a motion destination as a Cartesian Location, any differences in configuration are ignored.  However, if you specify a Angles Location as the destination for a Cartesian motion and the Angles correspond to a different configuration, this error will be signaled.  
        -1043 Asynchronous soft error A soft error signal from the servos or trajectory generator has been received by GPL, but no specific error code has been received. The error log entry immediately following this one normally indicates the actual error.

        Error signaling within the motion subsystem is a two-step process. When an error is first detected, a signal is sent to GPL immediately so that a controlled deceleration sequence can begin. This first signal generates a -1043 error code. Several milliseconds later, a more specific error code is sent to identify the source of the error. This second error code overwrites the -1043 error. Error -1043 is only seen if the error log is sampled after the initial error signal is received and before the specific error code is received.

        Unlike standard errors, a soft error does not disable robot power. 
        -1044 Auto mode disabled This indicates that:  (1) the Auto/Manual hardware input signal has been switched to Manual and motor power has been disabled or (2) an automatic program controlled motion was attempted, but the Auto/Manual hardware input signal is set to Manual.  When the Auto/Manual signal is set to Manual, only Jog ("Manual control") mode is permitted.  
        -1045 Soft E-STOP A soft E-Stop condition has been detected.  Any robot motion in progress is stopped rapidly but robot power is left on.  One of the following has occurred:

           1. The GPL property Controller.SoftEStop has been set to TRUE.
           2. The GPL console command SoftEStop has been executed.
           3. The digital input signal specified by "Soft E-Stop DIN" (DataID 246) has been asserted.
           4. The parameter database item "Soft E-Stop" (DataID 245) has been set to TRUE. 
        -1046 Power not enabled  An operation was attempted, such as trying to Attach to a robot, and power to the robot is required but has not yet been enabled.  Enable high power and repeat the operation. 

        -1047 Virtual MCP in Jog mode  An operation was attempted, such as trying to Attach to a robot, but the robot is already attached and is being controlled in Jog control mode via the web based Virtual MCP.  Go to the Web Virtual MCP, place the robot into computer control mode, and retry the operation. 

        -1048 Hardware MCP in Jog mode  An operation was attempted, such as trying to Attach to a robot, but the robot is already attached and is being controlled in Jog control mode via the hardware MCP.  Go to the MCP, place the robot into computer control mode, and retry the operation. 

        -1049 Timeout on homing DIN  During the homing operation, a digital input signal that is specified for a motor via the "Wait to home axis DIN" (DataID 2812) failed to turn on before the timeout period defined by the "Timeout on home axis, sec" (DataID 2813) expired.

        -1050 Illegal during joint motion  An operation or program instruction has been executed that requires that the robot either be stopped or moving in a Cartesian control mode.   However, the robot is executing a program controlled joint interpolated motion.  Wait for the joint interpolated motion to terminate before executing this instruction.

        -1051 Incorrect Cartesian trajectory mode  An operation or program instruction has been executed that requires that the Trajectory Generator be in a specific Cartesian mode, but the mode was incorrect.  Some possible problems could be:

            1. An instruction attempted to start the Real-time Trajectory Modification mode, but this mode is already active.
            2. An instruction attempted to set Real-time Trajectory Modification parameters, but this mode is not active. 

        -1052 Beyond conveyor limits Indicates that the position for a motion being planned is beyond the upstream or downstream limits of the referenced conveyor belt.  This error is generated in the following circumstances:

            1. A motion is being planned that is relative to a conveyor belt and the final position is projected to be out of the conveyors downstream limit before the robot can reach this position.  If a position is upstream of the upstream limit, the system pauses thread execution until the position comes within the limits and no error is generated. 

        -1053 Beyond conveyor limits while tracking  Indicates that a position is beyond the upstream or downstream limits of a conveyor belt while the robot is tracking the belt.  This error is generated in the following circumstances:

            1. The robot is moving relative to a conveyor belt and the final destination for the motion or the instantaneous position is beyond either limit of the conveyor belt.

        -1054 Can't attach Encoder Only robot An Auto Execution task (such as that for the Manual Control Pendant) or a GPL project has attempted to gain control of a robot by executing a Robot.Attach method or similar function, but the specified robot is an Encoder Only module. This type of kinematic module can be accessed to read the position of an encoder, but cannot be used to drive the encoder. Therefore, it is not legal to Attach this type of robot. Use the Robot.Selected method instead, if you only need read-only access to the robot. 

        -1055 Cartesian motion not configured  This error is generated if you attempt to perform a Cartesian motion either in manual control or program control mode, and some key Cartesian motion parameters have not been initialized.  Typically, this is caused by:

            1. The "100% Cartesian speeds" (DataID 2701) being set to 0 instead of their proper positive non-zero values.
           2.  The "100% Cartesian accels" (DataID 2703) being set to 0 instead of their proper positive non-zero values.  
        -1056 Incompatible robot position  The current robot position is not compatible with the requirements of the operation that has been initiated.  Situations where this error can be generated include:

            1. For the RPRR kinematic module, the two yaw axes have been defined to move at a fixed offset relative to each other.  However, at the start of a straight line motion, the yaw axes are not in proper alignment.


         * */
        #endregion

    }
}
