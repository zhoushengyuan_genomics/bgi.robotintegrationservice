﻿using BGI.Common.Logging;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BGI.RobotIntegrationService.Protocol
{
    public enum EEpsonRobotCmd
    {
        Home = 0,
        SetParameter,
        GetParameter,
        SetNextSlotID,
        GetNextSlotID,
        SetBiocReactionTime,
        GetBiocReactionTime,
        StartAOP,
        PauseAOP,
        ContinueAOP,
        StopAOP,
        R4ChipIn,
        Unloader2Scanner,
        Unloader2R3,
        Loader2R4,
        Loader2R1,
        GripperON,
        GripperOFF,
        GripperUP,
        GripperDN,
        MoveCylinder2R1,
        MoveCylinder2R2,
        MoveCylinder2R3,
        UpDnCylinder2Up,
        UpDnCylinder2Dn,
        TransChip,
        Chip2Imager,
        Chip2Loader,
        VacON,
        VacOFF,
        Chip2Recycler,
        ReleaseUnloader,
        Heartbeat,
        SetRecycleSensorEnable,
        GetRecycleSensorEnable,
        CChip2Loader,
        CChip2Imager,
        CatchChipAtSlotReliability,
        CatchChipAtImagerReliability,
        TransChipReliability,
        GoImagerReady,
        FeedChip2Loader,
        FeedChip2Imager,
        TeachImagerPosForceMotion,
        TeachPoint,
        SaveAsPoint,
        GetRecycleSlotState,
        ResetRecycleSlotState
    }

    public enum EEpsonRobotData
    {
        Unknow = 0,
        ERR,
        MTRIGER,
        IOStates,
        INFO,
        WARN,
        MIOStates,
        POS,
        Point,
        RecycleSlotState,
        RecycleSensorEnable,
        Param
    }

    public enum EEpsonRobotID
    {
        R1,
        R2,
        R3,
        R4,
        NULL
    }

    public class CEpsonRobotData
    {
        private string sender;
        private string senderIP;
        private EEpsonRobotData type;
        private string content;
        private DateTime recvTime;

        public string Sender
        {
            get
            {
                return sender;
            }
        }
        public string SenderIP
        {
            get
            {
                return senderIP;
            }
        }
        public EEpsonRobotData Type
        {
            get
            {
                return type;
            }
        }

        public string Content
        {
            get
            {
                return content;
            }
        }

        public DateTime RecvTime
        {
            get
            {
                return recvTime;
            }
        }

        public CEpsonRobotData(string sender, string senderIP, EEpsonRobotData type, string content, DateTime recvTime)
        {
            this.sender = sender;
            this.type = type;
            this.content = content;
            this.recvTime = recvTime;
            this.senderIP = senderIP;
        }
    }


    public class CEpsonRobotProtocol
    {
        private static object _LockObj = new object();
        public static string Terminator = Environment.NewLine;

        public static string GetSwitchOutputCmd(ushort outIdx, bool enable)
        {
            string ret = string.Empty;
            if (enable)
            {
                ret = string.Format("On {0}", outIdx);
            }
            else
            {
                ret = string.Format("Off {0}", outIdx);
            }

            return ret;
        }

        public static string StepMove(ushort axisID, ushort localID, double step)
        {
            string ret = string.Empty;
            switch(axisID)
            {
                case 1:
                    ret = string.Format("Move RealPos@({0}) +X({1})", localID, step);                    
                    break;
                case 2:
                    ret = string.Format("Move RealPos@({0}) +Y({1})", localID, step);
                    break;
                case 3:
                    ret = string.Format("Move RealPos@({0}) +Z({1})", localID, step);
                    break;
                case 4:
                    ret = string.Format("GO RealPos@({0}) +U({1})", localID, step);
                    break;
                case 5:
                    ret = string.Format("GO RealPos@({0}) +V({1})", localID, step);
                    break;
                case 6:
                    ret = string.Format("GO RealPos@({0}) +W({1})", localID, step);
                    break;
            }

            return ret;
        }

        public static string GetPosition(ushort localID)
        {
            string ret = string.Empty;
            if (localID < 16)
            {
                ret = string.Format("GetPosition:{0}", localID);
            }

            return ret;
        }

        public static string GetPoint(ushort pointID)
        {
            string ret = string.Empty;
            if (pointID < 1000)
            {
                ret = string.Format("plist {0}", pointID);
            }

            return ret;
        }

        public static string GetParameter(string pName)
        {
            return string.Format("print {0}", pName);
        }

        public static string SetParameter(string pName, string pValue)
        {
            return string.Format("{0}={1}", pName, pValue);
        }

        public static string TeachPoint(ushort pIdx, ushort localIdx)
        {            
            return (localIdx == 0 ? string.Format("P{0} = Here", pIdx) : string.Format("P{0} = Here @{1}", pIdx, localIdx));
        }

        public static string GoHome()
        {
            return "Home";
        }

        public static string Pause()
        {
            return "Pause";
        }

        public static List<ushort> GetPointIds(EEpsonRobotID rID)
        {
            List<ushort> pIds = new List<ushort>();
            if (rID == EEpsonRobotID.R1)
            {
                pIds.Add(0);
                pIds.Add(1);
                pIds.Add(2);
                pIds.Add(3);
                pIds.Add(4);
                pIds.Add(5);
                pIds.Add(6);
            }
            else if (rID == EEpsonRobotID.R2)
            {
                pIds.Add(7);
                pIds.Add(8);
                pIds.Add(9);
                pIds.Add(10);
                pIds.Add(11);
            }
            else if (rID == EEpsonRobotID.R3)
            {
                pIds.Add(12);
                pIds.Add(13);
                pIds.Add(14);
                pIds.Add(15);
                pIds.Add(16);
                pIds.Add(17);
                pIds.Add(18);
            }
            else if (rID == EEpsonRobotID.R4)
            {
                pIds.Add(70);
                pIds.Add(71);
                pIds.Add(72);
                pIds.Add(73);
                pIds.Add(80);
                pIds.Add(81);
                pIds.Add(82);
                pIds.Add(83);
                pIds.Add(90);
                pIds.Add(91);
                pIds.Add(92);
                pIds.Add(100);
                pIds.Add(101);
                pIds.Add(102);
                pIds.Add(103);
                pIds.Add(104);
                pIds.Add(110);
                pIds.Add(111);
                pIds.Add(112);
                pIds.Add(113);
                pIds.Add(114);
            }


            return pIds;
        }

        public static void ParseCmdResp(ref string strResp, ref Dictionary<string, string> cmdRespDict, out List<string> datas)
        {
            try
            {
                string cmdDot;
                string strCmd = string.Empty;
                string strValue = string.Empty;
                string aResp = string.Empty;
                datas = new List<string>();
                bool writeLineEnable = !(strResp.Contains(EEpsonRobotData.IOStates.ToString()) || strResp.Contains(EEpsonRobotCmd.Heartbeat.ToString()));
                if (writeLineEnable) Console.WriteLine("ParseCmdResp strResp[IN]:" + strResp);
                strResp = strResp.TrimStart(new char[2] { (char)10, (char)13 });
                while (strResp.IndexOf(Terminator) > 0)
                {
                    aResp = strResp.Substring(0, strResp.IndexOf(Terminator));
                    if (isCmdResp(aResp, out cmdDot))
                    {
                        strCmd = aResp.Substring(0, aResp.LastIndexOf(cmdDot));
                        strValue = aResp.Substring(aResp.LastIndexOf(cmdDot) + 1);
                        if (cmdRespDict.ContainsKey(strCmd))
                        {
                            cmdRespDict.Remove(strCmd);
                            Console.WriteLine(string.Format("{0} cmdRespDict Remove {1}", DateTime.Now.ToLongTimeString(), strCmd));
                        }                        
                        cmdRespDict.Add(strCmd, strValue);
                        Console.WriteLine(string.Format("{0} {1}:{2}", DateTime.Now.ToLongTimeString(), strCmd, strValue));
                    }
                    else
                    {                        
                        if (!datas.Contains(aResp)) datas.Add(aResp);
                    }
                    strResp = strResp.Substring(strResp.IndexOf(Terminator)).TrimStart(new char[2] { (char)10, (char)13 });
                }
                if (writeLineEnable) Console.WriteLine("ParseCmdResp strResp[OUT]:" + strResp);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static EEpsonRobotData ParseData(string strResp,out string content)
        {
            EEpsonRobotData dataType = EEpsonRobotData.Unknow;
            content = strResp;
            if (strResp.Contains(EEpsonRobotData.ERR.ToString())) dataType = EEpsonRobotData.ERR;
            else if (strResp.Contains(EEpsonRobotData.MIOStates.ToString())) dataType = EEpsonRobotData.MIOStates;
            else if (strResp.Contains(EEpsonRobotData.IOStates.ToString())) dataType = EEpsonRobotData.IOStates;
            else if (strResp.Contains(EEpsonRobotData.MTRIGER.ToString())) dataType = EEpsonRobotData.MTRIGER;
            else if (strResp.Contains(EEpsonRobotData.INFO.ToString())) dataType = EEpsonRobotData.INFO;
            else if (strResp.Contains(EEpsonRobotData.WARN.ToString())) dataType = EEpsonRobotData.WARN;
            else if (strResp.Contains(EEpsonRobotData.POS.ToString())) dataType = EEpsonRobotData.POS;
            else if (strResp.Contains(EEpsonRobotData.Point.ToString())) dataType = EEpsonRobotData.Point; 
            else if (strResp.Contains(EEpsonRobotData.RecycleSlotState.ToString())) dataType = EEpsonRobotData.RecycleSlotState;
            else if (strResp.Contains(EEpsonRobotData.RecycleSensorEnable.ToString())) dataType = EEpsonRobotData.RecycleSensorEnable;
            else if (strResp.Contains(EEpsonRobotData.Param.ToString())) dataType = EEpsonRobotData.Param;            

            if (dataType != EEpsonRobotData.Unknow) content = strResp.Substring(dataType.ToString().Length + 1);
            return dataType;
        }

        private static bool isGetCmdResp(string strResp)
        {
            if (strResp.Contains(EEpsonRobotCmd.GetParameter.ToString())
                || strResp.Contains(EEpsonRobotCmd.GetNextSlotID.ToString())
                || strResp.Contains(EEpsonRobotCmd.GetBiocReactionTime.ToString())
                || strResp.Contains(EEpsonRobotCmd.GetRecycleSensorEnable.ToString())
                || strResp.Contains(EEpsonRobotCmd.GetRecycleSlotState.ToString()))
            {
                return true;
            }

            return false;
        }

        public static bool isCmdResp(string strResp)
        {
            bool ret = false;
            foreach (string name in Enum.GetNames(typeof(EEpsonRobotCmd)))
            {
                //if (strResp.Contains(name))
                if (strResp.IndexOf(name) == 0)
                {
                    ret = true;
                    break;
                }
            }

            return ret;
        }

        private static bool isCmdResp(string strResp, out string cmdDot)
        {
            bool ret = false;
            cmdDot = string.Empty;
            if (isCmdResp(strResp))
            {
                ret = true;
                cmdDot = ":";
                if (isGetCmdResp(strResp)) cmdDot = "=";
            }

            return ret;
        }

        public static Dictionary<string, string> GetCmdQ(string cmd, Dictionary<string, string> parameters = null)
        {
            Dictionary<string, string> cmdQ = new Dictionary<string, string>();
            try
            {
                if (cmd == null || cmd == string.Empty) return cmdQ;

                string cmdKey = string.Empty;
                if (cmd == EEpsonRobotCmd.GetParameter.ToString()
                    || cmd == EEpsonRobotCmd.GetNextSlotID.ToString()
                    || cmd == EEpsonRobotCmd.GetBiocReactionTime.ToString()
                    || cmd == EEpsonRobotCmd.GetRecycleSensorEnable.ToString()
                    || cmd == EEpsonRobotCmd.GetRecycleSlotState.ToString())
                {
                    if (parameters != null)
                    {
                        foreach (var p in parameters) cmdQ.Add(string.Format("{0}:{1}", cmd, p.Key), null);
                    }
                }
                else if (cmd == EEpsonRobotCmd.SetParameter.ToString()
                    || cmd == EEpsonRobotCmd.SetNextSlotID.ToString()
                    || cmd == EEpsonRobotCmd.SetBiocReactionTime.ToString()
                    || cmd == EEpsonRobotCmd.R4ChipIn.ToString()
                    || cmd == EEpsonRobotCmd.Chip2Imager.ToString()
                    || cmd == EEpsonRobotCmd.Chip2Loader.ToString()
                    || cmd == EEpsonRobotCmd.VacON.ToString()
                    || cmd == EEpsonRobotCmd.VacOFF.ToString()
                    || cmd == EEpsonRobotCmd.Chip2Recycler.ToString()
                    || cmd == EEpsonRobotCmd.SetRecycleSensorEnable.ToString()
                    || cmd == EEpsonRobotCmd.GoImagerReady.ToString())
                {
                    if (parameters != null)
                    {
                        foreach (var p in parameters) cmdQ.Add(string.Format("{0}:{1}={2}", cmd, p.Key, p.Value), null);
                    }
                }
                else if (cmd == EEpsonRobotCmd.CChip2Imager.ToString()
                        || cmd == EEpsonRobotCmd.CChip2Loader.ToString()
                        || cmd == EEpsonRobotCmd.TransChip.ToString()
                        || cmd == EEpsonRobotCmd.CatchChipAtSlotReliability.ToString()
                        || cmd == EEpsonRobotCmd.CatchChipAtImagerReliability.ToString()
                        || cmd == EEpsonRobotCmd.TransChipReliability.ToString()
                        || cmd == EEpsonRobotCmd.FeedChip2Loader.ToString()
                        || cmd == EEpsonRobotCmd.FeedChip2Imager.ToString()
                        || cmd == EEpsonRobotCmd.TeachPoint.ToString()
                        || cmd == EEpsonRobotCmd.SaveAsPoint.ToString())
                {
                    string strParam = string.Empty;
                    if (parameters != null)
                    {
                        foreach (var p in parameters) strParam += (p.Value + "|");
                        strParam = strParam.Substring(0, strParam.LastIndexOf("|"));                        
                    }
                    cmdQ.Add(string.Format("{0}:{1}", cmd, strParam), null);
                }
                else
                {
                    cmdQ.Add(cmd, null);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return cmdQ;
        }

        public static int CheckCmdResult(string rCmd, Dictionary<string, string> cmdQ)
        {
            int ret = 0;
            try
            {
                if (!(rCmd == EEpsonRobotCmd.GetParameter.ToString()
                    || rCmd == EEpsonRobotCmd.GetNextSlotID.ToString()
                    || rCmd == EEpsonRobotCmd.GetBiocReactionTime.ToString()
                    || rCmd == EEpsonRobotCmd.GetRecycleSensorEnable.ToString()
                    || rCmd == EEpsonRobotCmd.GetRecycleSlotState.ToString()))
                {
                    foreach (var cmd in cmdQ)
                    {
                        if (cmd.Value != "0")
                        {
                            if (!int.TryParse(cmd.Value, out ret)) ret = -1;
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ret;
        }

        public static int CheckCmdResult(Dictionary<string, string> cmdQ)
        {
            int ret = 0;
            try
            {
                foreach (var cmd in cmdQ)
                {
                    if (!(cmd.Key.Contains(EEpsonRobotCmd.GetParameter.ToString())
                            || cmd.Key.Contains(EEpsonRobotCmd.GetNextSlotID.ToString())
                            || cmd.Key.Contains(EEpsonRobotCmd.GetBiocReactionTime.ToString())
                            || cmd.Key.Contains(EEpsonRobotCmd.GetRecycleSensorEnable.ToString())
                            || cmd.Key.Contains(EEpsonRobotCmd.GetRecycleSlotState.ToString()))
                         && cmd.Value != "0")
                    {
                        if (cmd.Value != "0")
                        {
                            if (!int.TryParse(cmd.Value, out ret)) ret = -1;
                            break;
                        }                        
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ret;
        }
    }
}
