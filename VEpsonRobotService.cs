﻿using BGI.Common.Comm;
using BGI.Common.Config;
using BGI.Common.Logging;
using BGI.RobotIntegrationService;
using BGI.RobotIntegrationService.Protocol;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace BGI.RobotIntegrationService
{
    public delegate void RobotDataReceivedHandler(CEpsonRobotData data);

    public class VEpsonRobotService
    {
        #region Variable
        public delegate void RobotConnectedHandler(string rClientIP, string deviceName);
        public event RobotConnectedHandler RobotCmdConnectedEvent;
        //public delegate void RobotDataReceivedHandler(CRobotData data);
        public event RobotDataReceivedHandler RobotDataReveivedEvent;
        public delegate void IOStateChangedHandler(string rClientIP, string ioStates);
        public event IOStateChangedHandler IOStateChangedEvent;

        public const ushort BIOC_SLOT_COUNT = 15;
        const ushort CMD_SERVER_PORT = 2000;
        const ushort DATA_SERVER_PORT = 3000;
        private static object _LockObj = new object();
        private static VEpsonRobotService _Instance = null;

        private Logger _Logger = LogMgr.GetLogger("VEpsonRobotService");

        private CTCPServerSocket cmdSocketServer = null;
        private CTCPServerSocket dataSocketServer = null;
        public Dictionary<string, string> RobotID2IPDict = new Dictionary<string, string>();
        public Dictionary<string, CEpsonRobotClient> ConnectedCmdSocketDict = new Dictionary<string, CEpsonRobotClient>();
        public Dictionary<string, CEpsonRobotClient> ConnectedDataSocketDict = new Dictionary<string, CEpsonRobotClient>();

        private IPAddress _ipAddress = null;
        private int _portCommand = 2000;
        private int _portData = 3000;
        public bool[] slotStates;
        public static VEpsonRobotService Get
        {
            get
            {
                if (_Instance == null)
                {
                    lock (_LockObj)
                    {
                        if (_Instance == null)
                        {
                            _Instance = new VEpsonRobotService();
                        }
                    }
                }
                return _Instance;
            }
        }
        
        public VEpsonRobotService()
        {
            slotStates = new bool[BIOC_SLOT_COUNT];
            checkPortAndKillOccupier(CMD_SERVER_PORT);
            checkPortAndKillOccupier(DATA_SERVER_PORT);
            SetupSocketServer();

            //_Logger.SetMinLogLevel(LogSeverityEnum.Info);
            _Logger.SetMinLogLevel(ConfigMgr.Get.GetCurValue<LogSeverityEnum>("System", "LogLevel"));
        }

        ~VEpsonRobotService()
        {
            checkPortAndKillOccupier(CMD_SERVER_PORT);
            checkPortAndKillOccupier(DATA_SERVER_PORT);
        }

        IDictionary<string, ushort> robotData2CheckSocketDict = new Dictionary<string, ushort>();
        public bool IsCmdSocketAvailable(string robotIP)
        {
            bool ret = false;
            try
            {
                #region CMD Method
                //foreach (var rClient in ConnectedCmdSocketDict)
                //{
                //    if (rClient.Key == robotIP)
                //    {
                //        Dictionary<string, string> cmdQ;
                //        ret = (rClient.Value.sentCmd2Robot(ECommand.Heartbeat.ToString(), out cmdQ) == 0);
                //        break;
                //    }
                //}
                #endregion CMD Method

                #region Check robot data method
                if (robotData2CheckSocketDict.ContainsKey(robotIP))
                {
                    ushort lastDataCount;
                    ushort crtDataCount;
                    lastDataCount = robotData2CheckSocketDict[robotIP];
                    Thread.Sleep(500);
                    crtDataCount = robotData2CheckSocketDict[robotIP];
                    ret = ((crtDataCount != lastDataCount) && ConnectedCmdSocketDict.ContainsKey(robotIP));
                }
                #endregion Check robot data method
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Fatal, ex.ToString());
            }
            //Console.WriteLine(string.Format("IsCmdSocketAvailable[{0}]...{1}", robotIP,ret));
            return ret;
        }

        public bool IsCmdSocketAvailable(string robotIP, int timeout = 1000)
        {
            bool result = false;
            try
            {
                if (robotData2CheckSocketDict.ContainsKey(robotIP) == false)
                    return false;

                DateTime timeSpan = DateTime.UtcNow.AddMilliseconds(timeout);
                AutoResetEvent _checkResetEvent = new AutoResetEvent(false);
                ushort lastDataCount = robotData2CheckSocketDict[robotIP];
                var robotCheckTask = Task.Run(() =>
                {
                    while (true)
                    {
                        if (_checkResetEvent.WaitOne(200) == true)
                            break;
                        if (timeSpan < DateTime.UtcNow)
                            break;

                        if (ConnectedCmdSocketDict.ContainsKey(robotIP) && (robotData2CheckSocketDict[robotIP] != lastDataCount))
                        {
                            result = true;
                            break;
                        }
                    }
                });

                if (robotCheckTask.Wait(timeout) == false)
                {
                    _Logger.Log()(LogSeverityEnum.Trace, "check device connect timeout.");
                    result = false;
                }
                _checkResetEvent.Set();
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Fatal, ex.ToString());
            }
            //Console.WriteLine(string.Format("IsCmdSocketAvailable[{0}]...{1}", robotIP, result));
            if(result == false)
                _Logger.Log()(LogSeverityEnum.Debug, "IsCmdSocketAvailable[{0}]...{1}", robotIP, result);
            return result;
        }

        public bool IsSlotFull(ushort slotIdx)
        {
            try
            {
                if (slotIdx >= BIOC_SLOT_COUNT) throw new Exception(string.Format("Error SlotIdx:{0}", slotIdx));

                //Console.WriteLine(string.Format("slot{0}:{1}", slotIdx, slotStates[slotIdx]));
                return slotStates[slotIdx];
            }
            catch(Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Fatal, ex.ToString());
                return false;
            }
        }
        #endregion

        #region _IPAddress socket
        public void SetupSocketServer(string ip = null, int portCMD = CMD_SERVER_PORT, int portData = DATA_SERVER_PORT)
        {
            try
            {
                if ((cmdSocketServer != null) && (dataSocketServer != null) 
                    && (_ipAddress.ToString() == ip) && (_portCommand == portCMD) && (_portData == portData))
                    return;

                if (cmdSocketServer != null)
                    cmdSocketServer.Close();
                if (dataSocketServer != null)
                    dataSocketServer.Close();

                _ipAddress = (ip != null && ip != string.Empty) ? IPAddress.Parse(ip): CTCPServerSocket.GetLocalIP4();
                _portCommand = (portCMD > 0) ? portCMD: CMD_SERVER_PORT;
                _portData = (portData > 0) ? portData : DATA_SERVER_PORT;
                cmdSocketServer = new CTCPServerSocket(_ipAddress, _portCommand);
                cmdSocketServer.ListenEvent += CmdSocketServer_ListenEvent;
                dataSocketServer = new CTCPServerSocket(_ipAddress, _portData);
                dataSocketServer.ListenEvent += DataSocketServer_ListenEvent;
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Fatal, ex.ToString());
            }
        }

        public bool StartAOP()
        {
            bool ret = false;
            try
            {
                // Check all robot connected-TBD

                foreach (var rClient in ConnectedCmdSocketDict)
                {
                    Dictionary<string, string> cmdQ;
                    rClient.Value.sentCmd2Robot(EEpsonRobotCmd.StartAOP.ToString(), out cmdQ);
                }
            }
            catch(Exception ex)
            {
                ret = false;
                _Logger.Log()(LogSeverityEnum.Fatal, ex.ToString());
            }

            return ret;
        }
        
        public int SentCmd2Robot(string robotIP, string rCmd, out Dictionary<string, string> cmdQ, Dictionary<string, string> parameters = null, int timeout = 60000)
        {
            int ret = -1;
            cmdQ = new Dictionary<string, string>();
            try
            {
                if (!ConnectedCmdSocketDict.ContainsKey(robotIP)) throw new Exception(string.Format("Unknow robotIP[{0}]", robotIP));

                return ConnectedCmdSocketDict[robotIP].sentCmd2Robot(rCmd,out cmdQ, parameters, timeout);
            }
            catch(Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Fatal, ex.ToString());
            }

            return ret;               
        }        
        public bool SwitchRobotOutput(string robotIP, ushort outIdx, bool enable)
        {
            bool ret = false;
            try
            {
                if (!ConnectedDataSocketDict.ContainsKey(robotIP)) throw new Exception(string.Format("Unknow robotIP[{0}]", robotIP));

                string strCmd = CEpsonRobotProtocol.GetSwitchOutputCmd(outIdx, enable);
                if (strCmd != string.Empty)
                    ret = ConnectedDataSocketDict[robotIP].sentCmd2Robot(strCmd);                
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Fatal, ex.ToString());
            }

            return ret;
        }

        public bool StepMove(string robotIP, ushort axisID, ushort localID, double step)
        {
            bool ret = false;
            try
            {
                string strCmd = CEpsonRobotProtocol.StepMove(axisID, localID, step);
                if (strCmd != string.Empty)
                    ret = ConnectedDataSocketDict[RobotID2IPDict[robotIP]].sentCmd2Robot(strCmd);
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Fatal, ex.ToString());
            }

            return ret;
        }

        public bool TeachWorkPos(string robotID, ushort pIdx, ushort localIdx = 0)
        {
            bool ret = false;
            try
            {
                Dictionary<string, string> cmdQ;
                Dictionary<string, string> param = new Dictionary<string, string>();
                param.Add("pointIdx", Convert.ToString(pIdx));
                param.Add("localIdx", Convert.ToString(localIdx));                 
                int errCode = VEpsonRobotService.Get.SentCmd2Robot(RobotID2IPDict[robotID], EEpsonRobotCmd.TeachPoint.ToString(), out cmdQ, param);
                ret = (errCode == 0);

                //ret = ConnectedDataSocketDict[RobotID2IPDict[robotID]].sentCmd2Robot(CEpsonRobotProtocol.TeachPoint(pIdx, localIdx));
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Fatal, ex.ToString());
            }

            return ret;
        }

        public bool SetParameter(string robotID, string pName, string pValue, ushort timeout = 1000)
        {
            bool ret = false;
            try
            {
                //Dictionary<string, string> cmdQ;
                //Dictionary<string, string> param = new Dictionary<string, string>();
                //param.Add(pName, pValue);
                //int errCode = VEpsonRobotService.Get.SentCmd2Robot(RobotID2IPDict[robotID], EEpsonRobotCmd.SetParameter.ToString(), out cmdQ, param);
                //ret = (errCode == 0);

                string strCmd = CEpsonRobotProtocol.SetParameter(pName, pValue);
                if (strCmd != string.Empty)
                    ret = ConnectedDataSocketDict[RobotID2IPDict[robotID]].sentCmd2Robot(strCmd);
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Fatal, ex.ToString());
            }

            return ret;
        }

        public string GetParameter(string robotID, string pName, ushort timeout = 1000)
        {
            string ret = string.Empty;
            try
            {
                //Dictionary<string, string> cmdQ;
                //Dictionary<string, string> param = new Dictionary<string, string>();
                //param.Add(pName, null);
                //int errCode = VEpsonRobotService.Get.SentCmd2Robot(RobotID2IPDict[robotID], EEpsonRobotCmd.GetParameter.ToString(), out cmdQ, param, timeout);
                //return (errCode == 0 ? cmdQ[string.Format("{0}:{1}", EEpsonRobotCmd.GetParameter.ToString(), pName)]:ret);

                string strCmd = CEpsonRobotProtocol.GetParameter(pName);
                if (strCmd != string.Empty)
                {
                    robotParamDict.Clear();
                    if (ConnectedDataSocketDict[RobotID2IPDict[robotID]].sentCmd2Robot(strCmd))
                    {
                        ushort intv = 10;
                        int adds = 0;
                        int count = timeout / intv;
                        while(adds < count)
                        {
                            if (robotParamDict.ContainsKey(pName)) return robotParamDict[pName].Trim();
                            adds++;
                            Thread.Sleep(intv);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Fatal, ex.ToString());
            }

            return ret;
        }

        public bool SaveAsWorkPos(string robotID, ushort sPointIdx, ushort tPointIdx)
        {
            bool ret = false;
            try
            {
                Dictionary<string, string> cmdQ;
                Dictionary<string, string> param = new Dictionary<string, string>();
                param.Add("sPointIdx", Convert.ToString(sPointIdx));
                param.Add("tPointIdx", Convert.ToString(tPointIdx));
                int errCode = VEpsonRobotService.Get.SentCmd2Robot(RobotID2IPDict[robotID], EEpsonRobotCmd.SaveAsPoint.ToString(), out cmdQ, param);
                ret = (errCode == 0);
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Fatal, ex.ToString());
            }

            return ret;
        }

        public bool GoHome(string robotID)
        {
            bool ret = false;
            try
            {
                Dictionary<string, string> cmdQ;
                int errCode = SentCmd2Robot(RobotID2IPDict[robotID], EEpsonRobotCmd.Home.ToString(), out cmdQ);
                ret = (errCode == 0);
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Fatal, ex.ToString());
            }

            return ret;
        }

        public bool GripperSwitch(string robotID, bool enable)
        {
            bool ret = false;
            try
            {
                Dictionary<string, string> cmdQ;
                EEpsonRobotCmd eCmd = enable ? EEpsonRobotCmd.GripperON : EEpsonRobotCmd.GripperOFF;
                int errCode = VEpsonRobotService.Get.SentCmd2Robot(RobotID2IPDict[robotID], eCmd.ToString(), out cmdQ);
                ret = (errCode == 0);
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Fatal, ex.ToString());
            }

            return ret;
        }

        public bool GetPoint(string robotIP, ushort pointID)
        {
            bool ret = false;
            try
            {
                string strCmd = CEpsonRobotProtocol.GetPoint(pointID);
                if (strCmd != string.Empty)
                    ret = ConnectedDataSocketDict[RobotID2IPDict[robotIP]].sentCmd2Robot(strCmd);
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Fatal, ex.ToString());
            }

            return ret;
        }

        public List<ushort> GetPointIds(string robotIP)
        {
            List<ushort> ret = new List<ushort>();
            try
            {
                EEpsonRobotID rID = (EEpsonRobotID)Enum.Parse(typeof(EEpsonRobotID), robotIP); //GetRobotID(robotIP);
                if (rID != EEpsonRobotID.NULL)
                    ret = CEpsonRobotProtocol.GetPointIds(rID);
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Fatal, ex.ToString());
            }

            return ret;
        }

        public EEpsonRobotID GetRobotID(string rIP)
        {
            EEpsonRobotID rID = EEpsonRobotID.NULL;
            try
            {
                string rName = string.Empty;
                foreach (var obj in VEpsonRobotService.Get.RobotID2IPDict)
                {
                    if (obj.Value == rIP)
                    {
                        rID = (EEpsonRobotID)Enum.Parse(typeof(EEpsonRobotID), obj.Key);
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Fatal, ex.ToString());
            }

            return rID;
        }

        public bool GetPosition(string robotIP, ushort localID)
        {
            bool ret = false;
            try
            {
                string strCmd = CEpsonRobotProtocol.GetPosition(localID);
                if (strCmd != string.Empty)
                    ret = ConnectedDataSocketDict[RobotID2IPDict[robotIP]].sentCmd2Robot(strCmd);
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Fatal, ex.ToString());
            }

            return ret;
        }

        private void CmdSocketServer_ListenEvent(CConnectedSocket asySocket)
        {
            try
            {
                CEpsonRobotClient rClient;
                var remoteIp = asySocket.RemoteIP;
                lock (ConnectedCmdSocketDict)
                {
                    rClient = new CEpsonRobotClient(asySocket);
                    if (ConnectedCmdSocketDict.ContainsKey(remoteIp))
                    {
                        ConnectedCmdSocketDict[remoteIp].Close();
                        ConnectedCmdSocketDict[remoteIp] = rClient;                        
                    }
                    else
                    {
                        ConnectedCmdSocketDict.Add(remoteIp, rClient);
                    }

                    //if (RobotCmdConnectedEvent != null)
                    //    RobotCmdConnectedEvent(remoteIp, rClient.RobotID);
                    _Logger.Log()(LogSeverityEnum.Info, "cmd reveiced. {0}.", remoteIp);
                }
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Fatal, ex.ToString());
            }
        }

        private Dictionary<string, string> robotParamDict = new Dictionary<string, string>();
        private Dictionary<string, string> robotIOStatesDict = new Dictionary<string, string>();
        private void RClient_ReceivedDataEvent(CEpsonRobotData data)
        {
            try
            {
                if (data == null) return;

                if (!robotData2CheckSocketDict.ContainsKey(data.SenderIP))
                    robotData2CheckSocketDict.Add(data.SenderIP, ushort.MinValue);
                if (robotData2CheckSocketDict[data.SenderIP] == ushort.MaxValue)
                    robotData2CheckSocketDict[data.SenderIP] = ushort.MinValue;
                else
                    robotData2CheckSocketDict[data.SenderIP] += 1;

                if (data.Type == EEpsonRobotData.MIOStates)
                {                    
                    if (data.Sender == EEpsonRobotID.R1.ToString())
                    {
                        for (int i = 0; i <= 5; i++)
                            slotStates[i] = (data.Content.Substring(i, 1) == "0") ? false : true;
                    }
                    else if (data.Sender == EEpsonRobotID.R2.ToString())
                    {
                        for (int i = 7; i <= 10; i++)
                            slotStates[i-1] = (data.Content.Substring(i, 1) == "0") ? false : true;
                    }
                    else if (data.Sender == EEpsonRobotID.R3.ToString())
                    {
                        for (int i = 12; i <= 16; i++)
                            slotStates[i - 2] = (data.Content.Substring(i, 1) == "0") ? false : true;
                    }
                }
                else if (data.Type == EEpsonRobotData.IOStates)
                {
                    string[] ioStates = data.Content.Split('|');
                    if (ioStates.Length == 2)
                    {
                        string pOutIOStates = data.Sender + "OUTIOStates";
                        if (!robotIOStatesDict.ContainsKey(pOutIOStates))
                            robotIOStatesDict.Add(pOutIOStates, string.Empty);
                        if (robotIOStatesDict[pOutIOStates] != string.Empty && robotIOStatesDict[pOutIOStates] != ioStates[1])
                        {
                            TriggerIOStatesChanged(ioStates[1], robotIOStatesDict[pOutIOStates], data.SenderIP);
                        }
                        robotIOStatesDict[pOutIOStates] = ioStates[1];
                    }
                }
                else if (data.Type == EEpsonRobotData.Param)
                {
                    string[] pms = data.Content.Split('=');
                    if (pms.Length == 2 && !robotParamDict.ContainsKey(pms[0])) robotParamDict.Add(pms[0], pms[1]);
                }

                if (!RobotID2IPDict.ContainsKey(data.Sender)) RobotID2IPDict.Add(data.Sender, data.SenderIP);
                if (RobotDataReveivedEvent != null)
                    RobotDataReveivedEvent(data);
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Fatal, ex.ToString());
            }
        }

        private void TriggerIOStatesChanged(string states,string preStates,string robotIP)
        {
            try
            {
                if (states.Length != preStates.Length) return;
                
                string lstState;
                string crtState;
                string outputStr = string.Empty;
                for (int i = 0; i < states.Length; i++)
                {
                    crtState = states.Substring(i, 1);
                    lstState = preStates.Substring(i, 1);
                    if (crtState != lstState)
                    {
                        outputStr += string.Format("{0}_{1},", i, crtState);
                    }
                }
                if (outputStr != string.Empty)
                {
                    outputStr = outputStr.Substring(0, outputStr.Length - 1);
                    //Console.WriteLine(string.Format("outputStr:{0}", outputStr));
                    if (IOStateChangedEvent != null)
                        IOStateChangedEvent(robotIP, outputStr);
                }
            }
            catch(Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Fatal, ex.ToString());
            }
        }

        private void DataSocketServer_ListenEvent(CConnectedSocket asySocket)
        {
            try
            {
                CEpsonRobotClient rClient;
                var remoteIp = asySocket.RemoteIP;
                lock (ConnectedDataSocketDict)
                {
                    rClient = new CEpsonRobotClient(asySocket);
                    rClient.ReceivedDataEvent += RClient_ReceivedDataEvent;
                    if (ConnectedDataSocketDict.ContainsKey(remoteIp))
                    {
                        ConnectedDataSocketDict[remoteIp].Close();                        
                        ConnectedDataSocketDict[remoteIp] = rClient;
                        
                    }
                    else
                    {
                        ConnectedDataSocketDict.Add(remoteIp, rClient);
                    }
                }

                //if (RobotDataConnectedEvent != null)
                //    RobotDataConnectedEvent(remoteIp);
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Fatal, ex.ToString());
            }
        }

        public void CloseSocketServer()
        {
            try
            {
                _Logger.Log()(LogSeverityEnum.Info, "CloseSocketServer...");
                if (cmdSocketServer != null)
                {
                    cmdSocketServer.Close();
                    cmdSocketServer = null;
                }
                if (dataSocketServer != null)
                {
                    dataSocketServer.Close();
                    dataSocketServer = null;
                }
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Fatal, ex.ToString());
            }
        }

        #endregion

        /// <summary>
        /// Check port's state and kill its occupier
        /// </summary>
        /// <param name="port"></param>
        private void checkPortAndKillOccupier(ushort port)
        {
            try
            {
                Process pro = new Process();
                pro.StartInfo.FileName = "cmd.exe";
                pro.StartInfo.UseShellExecute = false;
                pro.StartInfo.RedirectStandardInput = true;
                pro.StartInfo.RedirectStandardOutput = true;
                pro.StartInfo.RedirectStandardError = true;
                pro.StartInfo.CreateNoWindow = true;
                pro.Start();
                pro.StandardInput.WriteLine("netstat -ano");
                pro.StandardInput.WriteLine("exit");

                string line = null;
                Regex reg = new Regex("\\s+", RegexOptions.Compiled);
                while ((line = pro.StandardOutput.ReadLine()) != null)
                {
                    line = line.Trim();
                    if (line.StartsWith("TCP", StringComparison.OrdinalIgnoreCase))
                    {
                        line = reg.Replace(line, ",");
                        string[] arr = line.Split(',');
                        if (arr[1].EndsWith(":" + port.ToString()))
                        {
                            int pid = Int32.Parse(arr[4]);
                            Process proOccupier = Process.GetProcessById(pid);
                            proOccupier.Kill();
                            break;
                        }
                    }
                }
                pro.Close();
            }
            catch(Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Fatal, ex.ToString());
            }
        }
    }
}
