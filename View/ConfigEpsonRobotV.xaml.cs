﻿using BGI.RobotIntegrationService.EpsonRobot;
using BGI.RobotIntegrationService.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BGI.RobotIntegrationService.View
{
    /// <summary>
    /// Interaction logic for ConfigEpsonRobotV.xaml
    /// </summary>
    public partial class ConfigEpsonRobotV : Window
    {
        public ConfigEpsonRobotV()
        {
            InitializeComponent();
            this.DataContext = new ConfigEpsonRobotVM();
        }

        public ConfigEpsonRobotV(double parentPosTop, double parentPosLeft, double parentPosWidth, double parentPosHeight)
        {
            InitializeComponent();

            double scaleWidth = 0.8;
            double scaleHeight = 0.8;
            this.Top = parentPosTop + parentPosWidth * scaleWidth / 2;
            this.Left = parentPosLeft + parentPosHeight * scaleHeight / 2;
            this.Width = parentPosWidth * scaleWidth;
            this.Height = parentPosHeight * scaleHeight;
            this.DataContext = new ConfigEpsonRobotVM();
            this.comboBoxLocal.SelectedIndex = 0;
            this.comboBoxMotionMode.SelectedIndex = 0;
        }

        private void comboBoxMode_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            //EMotionMode motionMode;
            if (comboBoxMotionMode.SelectedValue.ToString() == "Local")
            {
                buttonJ1Up.Content = "X+";
                buttonJ1Dn.Content = "X-";
                buttonJ2Up.Content = "Y+";
                buttonJ2Dn.Content = "Y-";
                buttonJ3Up.Content = "Z+";
                buttonJ3Dn.Content = "Z-";
                buttonJ4Up.Content = "U+";
                buttonJ4Dn.Content = "U-";
                buttonJ5Up.Content = "V+";
                buttonJ5Dn.Content = "V-";
                buttonJ6Up.Content = "W+";
                buttonJ6Dn.Content = "W-";
                //motionMode = EMotionMode.Local;
            }
            else
            {
                buttonJ1Up.Content = "J+";
                buttonJ1Dn.Content = "J-";
                buttonJ2Up.Content = "J+";
                buttonJ2Dn.Content = "J-";
                buttonJ3Up.Content = "J+";
                buttonJ3Dn.Content = "J-";
                buttonJ4Up.Content = "J+";
                buttonJ4Dn.Content = "J-";
                buttonJ5Up.Content = "J+";
                buttonJ5Dn.Content = "J-";
                buttonJ6Up.Content = "J+";
                buttonJ6Dn.Content = "J-";
                //motionMode = EMotionMode.Joint;
            }
            //(this.DataContext as ConfigEpsonRobotVM).SetMotionMode(motionMode);
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            (this.DataContext as ConfigEpsonRobotVM).TimerEnable(false);
        }

        private void dataGridParameters_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {            
            int rowIdx = e.Row.GetIndex();
            int colIdx = e.Column.DisplayIndex;
            var pValueNew = (e.EditingElement as TextBox).Text;
            if (pValueNew == string.Empty) return;
            var pName = (dataGridParameters.Columns[colIdx - 2].GetCellContent(dataGridParameters.Items[rowIdx]) as TextBlock).Text;
            var pValue = (dataGridParameters.Columns[colIdx - 1].GetCellContent(dataGridParameters.Items[rowIdx]) as TextBlock).Text;
            if (MessageBoxResult.Yes == MessageBox.Show(string.Format("Do you want to update parameter[{0}]'s value[{1} to {2}]?", pName, pValue, pValueNew), "Question", MessageBoxButton.YesNo, MessageBoxImage.Question)
                && (pName != "APPVersion"))
            {
                // Check parameter's value  -TBD                
                bool ret = VEpsonRobotService.Get.SetParameter(EpsonRobotConfig.Instance.RobotID, pName, pValueNew);
                if (ret)
                {
                    string mValue = VEpsonRobotService.Get.GetParameter(EpsonRobotConfig.Instance.RobotID, pName);
                    if (mValue != string.Empty) (this.DataContext as ConfigEpsonRobotVM).UpdateParameterList(pName, mValue);
                    ret = (mValue.Trim() == pValueNew);
                }
                MessageBox.Show(string.Format("Update parameter[{0}]'s value[{1} to {2}] {3}!", pName, pValue, pValueNew, ret ? "successe" : "fail"));
            }
        }

        private void dataGridWorkpos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            (this.DataContext as ConfigEpsonRobotVM).SelectedPosId = dataGridWorkpos.SelectedIndex;
            Console.WriteLine(string.Format("SelectedPosId:{0}", (this.DataContext as ConfigEpsonRobotVM).SelectedPosId));
        }
    }
}
