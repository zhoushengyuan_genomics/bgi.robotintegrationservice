﻿using BGI.RobotIntegrationService.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BGI.RobotIntegrationService.View
{
    /// <summary>
    /// Interaction logic for ConfigPreciseRobotV.xaml
    /// </summary>
    public partial class ConfigPreciseRobotV : Window
    {
        public ConfigPreciseRobotV()
        {
            InitializeComponent();
            
            this.DataContext = new ConfigPreciseRobotVM();
        }

        private void dataGridWorkflowMotion_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            if (e.Column.DisplayIndex > 1 && e.Column.DisplayIndex < 8)
            {
                float fvalue;
                if (float.TryParse((e.EditingElement as TextBox).Text, out fvalue))
                    (this.DataContext as ConfigPreciseRobotVM).UpdateWorkflowMotionWorkpos(e.Row.GetIndex(), e.Column.DisplayIndex - 2, fvalue);
            }
            
            
            Console.WriteLine(string.Format("CellEdit[{0},{1}]:{2}", e.Row.GetIndex(), e.Column.DisplayIndex, (e.EditingElement as TextBox).Text));
        }

        private void dataGridWorkflowMotion_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            (this.DataContext as ConfigPreciseRobotVM).SelectedWorkflowMotionIdx = dataGridWorkflowMotion.SelectedIndex;
        }
    }
}
