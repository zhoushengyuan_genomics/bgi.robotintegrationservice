﻿using BGI.Common.Logging;
using BGI.RobotIntegrationService.EpsonRobot;
using BGI.RobotIntegrationService.Protocol;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace BGI.RobotIntegrationService.ViewModel
{
    class ConfigDipSeqRecyclerVM : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private Logger _Logger = LogMgr.GetLogger("ConfigDipSeqRecyclerVM");

        public ObservableCollection<string> RecycleSensorEnableColor { get; set; }
        public ObservableCollection<string> RecycleSlotStateColor { get; set; }

        private string robotID;
        public ConfigDipSeqRecyclerVM()
        {
            robotID = EpsonRobotConfig.Instance.RobotID;
            RecycleSlotStateColor = new ObservableCollection<string>();
            for (int i = 0; i < 4; i++) RecycleSlotStateColor.Add("RED");
            RecycleSensorEnableColor = new ObservableCollection<string>();
            for (int i = 0; i < 4; i++) RecycleSensorEnableColor.Add("RED");
            VEpsonRobotService.Get.RobotDataReveivedEvent += new RobotDataReceivedHandler(this.EpsonRobotService_RobotDataReveivedEvent);
        }

        private void EpsonRobotService_RobotDataReveivedEvent(CEpsonRobotData data)
        {
            try
            {
                if (data.Type == EEpsonRobotData.RecycleSensorEnable)
                    updateRecycleSensorEnables(data);
                else if (data.Type == EEpsonRobotData.RecycleSlotState)
                    updateRecycleSlotStates(data);
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Error, "{0}", ex);
            }
        }

        private void updateRecycleSlotStates(CEpsonRobotData data)
        {
            try
            {
                if (data.Type == EEpsonRobotData.RecycleSlotState)
                {
                    for (int i = 0; i < data.Content.Length; i++) RecycleSlotStateColor[i] = (data.Content.Substring(i, 1) == "1" ? "RED" : "GREEN");
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("RecycleSlotStateColor"));
                    }
                }
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Error, "{0}", ex);
            }
        }

        private void updateRecycleSensorEnables(CEpsonRobotData data)
        {
            try
            {
                if (data.Type == EEpsonRobotData.RecycleSensorEnable)
                {
                    for (int i = 0; i < data.Content.Length; i++) RecycleSensorEnableColor[i] = (data.Content.Substring(i, 1) == "1" ? "GREEN" : "RED");
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("RecycleSensorEnableColor"));
                    }
                }
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Error, "{0}", ex);
            }
        }

        public ICommand btnClick
        {
            get
            {
                return new ViewCommand(btnClickResp, btnClickCheck);
            }
        }

        public bool btnClickCheck()
        {
            return true;
        }
        public void btnClickResp(object sender)
        {
            try
            {
                if (sender is Button)
                {
                    var btn = sender as Button;
                    if (btn.Name.IndexOf("ResetRecycleSlot") > 0)
                    {                        
                        if (MessageBoxResult.No == MessageBox.Show("Do you want to reset this recycler's state?", "Question", MessageBoxButton.YesNo, MessageBoxImage.Question)) return;

                        ushort slotIdx = ushort.Parse(btn.Name.Substring(btn.Name.IndexOf("ResetRecycleSlot") + "ResetRecycleSlot".Length));
                        VEpsonRobotService.Get.SetParameter(robotID, string.Format("RecycleSlotState({0})", slotIdx), "0");
                    }
                    else if (btn.Name.IndexOf("EnableRecycleSensor") > 0)
                    {
                        ushort enableValue = 0;
                        ushort slotIdx = ushort.Parse(btn.Name.Substring(btn.Name.IndexOf("EnableRecycleSensor") + "EnableRecycleSensor".Length));
                        if (RecycleSensorEnableColor[slotIdx] == "RED") enableValue = 1;                        
                        if (MessageBoxResult.No == MessageBox.Show(string.Format("Do you want to {0} this recycler?", enableValue==1?"Enable":"Disable"), "Question", MessageBoxButton.YesNo, MessageBoxImage.Question)) return;

                        VEpsonRobotService.Get.SetParameter(robotID, string.Format("RecycleSeneorEnable({0})", slotIdx), enableValue.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Error, "{0}", ex);
            }
            finally
            {

            }
        }
    }
}
