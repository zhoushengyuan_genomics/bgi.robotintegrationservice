﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using System.ComponentModel;
using System.Windows.Controls;
using System.Windows.Input;
using BGI.Common.Logging;
using System.Collections.ObjectModel;
using BGI.RobotIntegrationService.EpsonRobot;
using System.Threading;
using System.Windows;
using BGI.RobotIntegrationService.Protocol;
using BGI.RobotIntegrationService.DataType;

namespace BGI.RobotIntegrationService.ViewModel
{
    class ConfigEpsonRobotVM : INotifyPropertyChanged
    {
        public const ushort TEACH_TEMP_POSID = 120;
        public const ushort IMAGER_BASE_POSID = 100;

        public event PropertyChangedEventHandler PropertyChanged;
        private System.Timers.Timer positionTimer = new System.Timers.Timer();
        private Logger _Logger = LogMgr.GetLogger("ConfigPreciseRobotVM");

        public float StepDist
        {
            get
            {
                return EpsonRobotConfig.Instance.StepDist;
            }

            set
            {
                EpsonRobotConfig.Instance.StepDist = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("StepDist"));
            }
        }              

        private string robotScript;
        public string RobotScript
        {
            get
            {
                return robotScript;
            }

            set
            {
                robotScript = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("RobotScript"));
            }
        }
        private string cmdParameter;
        public string CmdParameter
        {
            get
            {
                return cmdParameter;
            }

            set
            {
                cmdParameter = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("CmdParameter"));
            }
        }
        private double j1Pos;
        public double J1Pos
        {
            get
            {
                return j1Pos;
            }

            set
            {
                j1Pos = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("J1Pos"));
            }
        }
        private double j2Pos;
        public double J2Pos
        {
            get
            {
                return j2Pos;
            }

            set
            {
                j2Pos = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("J2Pos"));
            }
        }
        private double j3Pos;
        public double J3Pos
        {
            get
            {
                return j3Pos;
            }

            set
            {
                j3Pos = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("J3Pos"));
            }
        }
        private double j4Pos;
        public double J4Pos
        {
            get
            {
                return j4Pos;
            }

            set
            {
                j4Pos = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("J4Pos"));
            }
        }
        private double j5Pos;
        public double J5Pos
        {
            get
            {
                return j5Pos;
            }

            set
            {
                j5Pos = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("J5Pos"));
            }
        }
        private double j6Pos;
        public double J6Pos
        {
            get
            {
                return j6Pos;
            }

            set
            {
                j6Pos = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("J6Pos"));
            }
        }
        private string _J1Flag;
        public string J1Flag
        {
            get
            {
                return _J1Flag;
            }

            set
            {
                _J1Flag = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("J1Flag"));
            }
        }
        private string _J4Flag;
        public string J4Flag
        {
            get
            {
                return _J4Flag;
            }

            set
            {
                _J4Flag = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("J4Flag"));
            }
        }
        private string _J6Flag;
        public string J6Flag
        {
            get
            {
                return _J6Flag;
            }

            set
            {
                _J6Flag = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("J6Flag"));
            }
        }
        private string _Hand;
        public string Hand
        {
            get
            {
                return _Hand;
            }

            set
            {
                _Hand = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("Hand"));
            }
        }
        private string _Elbow;
        public string Elbow
        {
            get
            {
                return _Elbow;
            }

            set
            {
                _Elbow = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("Elbow"));
            }
        }
        private string _Wrist;
        public string Wrist
        {
            get
            {
                return _Wrist;
            }

            set
            {
                _Wrist = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("Wrist"));
            }
        }

        bool _SaveAsWorkPosEnable;
        public bool SaveAsWorkPosEnable
        {
            get
            {
                return _SaveAsWorkPosEnable;
            }

            set
            {
                _SaveAsWorkPosEnable = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("SaveAsWorkPosEnable"));
            }
        }

        public ObservableCollection<EpsonRobotConfigLocal> LocalList { get; set; }
        public ObservableCollection<EpsonRobotConfigMotionMode> MotionModeList { get; set; }
        public ObservableCollection<EpsonRobotConfigCmd> RobotCmdList { get; set; }
        public ObservableCollection<EpsonRobotConfigMotionParameter> MotionParameterList { get; set; }
        public ObservableCollection<IO> IOInputList { get; set; }
        public ObservableCollection<IO> IOOutputList { get; set; }
        public ObservableCollection<bool> IOInputStates { get; set; }
        public ObservableCollection<bool> IOOutputStates { get; set; }
        public ObservableCollection<WorkPos> WorkPosList { get; set; }
        public ObservableCollection<EpsonRobotConfigMIO> MIOList { get; set; }
        public ObservableCollection<bool> MIOStates { get; set; }

        private string robotID;
        private ushort selectedLocal;
        private EpsonRobotConfigCmd selectedCmd;
        public int SelectedPosId = int.MinValue;
        public ConfigEpsonRobotVM()
        {
            robotID = EpsonRobotConfig.Instance.RobotID;
            IOInputList = new ObservableCollection<IO>();
            IOOutputList = new ObservableCollection<IO>();
            IOInputStates = new ObservableCollection<bool>();
            for (int i = 0; i < EpsonRobotConfig.Instance.InIOCount; i++) IOInputStates.Add(false);
            IOOutputStates = new ObservableCollection<bool>();
            for (int i = 0; i < EpsonRobotConfig.Instance.OutIOCount; i++) IOOutputStates.Add(false);
            WorkPosList = new ObservableCollection<WorkPos>();
            LocalList = new ObservableCollection<EpsonRobotConfigLocal>();
            RobotCmdList = new ObservableCollection<EpsonRobotConfigCmd>();
            MotionModeList = new ObservableCollection<EpsonRobotConfigMotionMode>();
            MotionParameterList = new ObservableCollection<EpsonRobotConfigMotionParameter>();
            foreach (var mp in EpsonRobotConfig.Instance.MotionParameter) MotionParameterList.Add(mp);
            foreach (var lc in EpsonRobotConfig.Instance.Local) LocalList.Add(lc);
            foreach (var mm in EpsonRobotConfig.Instance.MotionMode) MotionModeList.Add(mm);
            foreach (var rc in EpsonRobotConfig.Instance.Cmd) RobotCmdList.Add(rc);
            foreach (var io in EpsonRobotConfig.Instance.IO)
            {
                if (io.Dir == IODir.@in)
                    IOInputList.Add(io);
                else
                    IOOutputList.Add(io);
            }
            MIOStates = new ObservableCollection<bool>();
            MIOList = new ObservableCollection<EpsonRobotConfigMIO>();
            foreach (var mio in EpsonRobotConfig.Instance.MIO)
            {
                MIOList.Add(mio);
                MIOStates.Add(false);
            }
            foreach (var wp in EpsonRobotConfig.Instance.WorkPos) WorkPosList.Add(wp);
            VEpsonRobotService.Get.RobotDataReveivedEvent += new RobotDataReceivedHandler(this.EpsonRobotService_RobotDataReveivedEvent);
            positionTimer.Enabled = true;
            positionTimer.Interval = 1000;
            positionTimer.Start();
            positionTimer.Elapsed += new System.Timers.ElapsedEventHandler(positionTimerHandler);
            //defineParameterList(robotID);
            refreshParameterList();
            refreshPointList();
        }

        public void TimerEnable(bool enable)
        {
            positionTimer.Enabled = enable;
        }

        private void positionTimerHandler(object source, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                lock (this)
                {
                    if (robotID != string.Empty)
                    {
                        VEpsonRobotService.Get.GetPosition(robotID, selectedLocal);
                    }
                }
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Error, "{0}", ex);
            }
        }

        private Dictionary<string, string> getRobotParameterCmds(string[] paramNames)
        {
            Dictionary<string, string> cmdQ = new Dictionary<string, string>();
            try
            {
                Dictionary<string, string> getParameters = new Dictionary<string, string>();
                foreach (var obj in paramNames) getParameters.Add(obj, null);
                var getParameterCmds = CEpsonRobotProtocol.GetCmdQ(EEpsonRobotCmd.GetParameter.ToString(), getParameters);
                foreach (var cmd in getParameterCmds) cmdQ.Add(cmd.Key, cmd.Value);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return cmdQ;
        }

        private void defineParameterList(string robotID)
        {
            try
            {
                List<string> pnames = new List<string>();
                foreach (var mp in MotionParameterList) pnames.Add(mp.Name);

                var cmdQ = getRobotParameterCmds(pnames.ToArray());
                if (VEpsonRobotService.Get.ConnectedCmdSocketDict[VEpsonRobotService.Get.RobotID2IPDict[robotID]].sentCmd2Robot(ref cmdQ) == 0)
                {
                    MotionParameterList = new ObservableCollection<EpsonRobotConfigMotionParameter>();
                    foreach (var cmd in cmdQ)
                    {
                        var mp = new EpsonRobotConfigMotionParameter();
                        mp.Name = cmd.Key.Substring(cmd.Key.IndexOf(":") + 1);
                        mp.Value = cmd.Value;
                        MotionParameterList.Add(mp);
                    }
                    if (PropertyChanged != null)
                        PropertyChanged(this, new PropertyChangedEventArgs("MotionParameterList"));
                }
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Error, "{0}", ex);
            }
        }

        public void UpdateParameterList(string pName, string pValue)
        {
            try
            {
                EpsonRobotConfigMotionParameter[] mparameters = new EpsonRobotConfigMotionParameter[MotionParameterList.Count]; 
                MotionParameterList.CopyTo(mparameters, 0);
                int pIdx = -1;
                for (int i = 0; i < mparameters.Length; i++)
                {
                    if (mparameters[i].Name == pName)
                    {
                        pIdx = i;
                        break;
                    }
                }

                if (pIdx != -1 && PropertyChanged != null)
                {
                    mparameters[pIdx].Value = pValue;
                    MotionParameterList = new ObservableCollection<EpsonRobotConfigMotionParameter>();
                    foreach (var mp in mparameters) MotionParameterList.Add(mp);
                    PropertyChanged(this, new PropertyChangedEventArgs("MotionParameterList"));
                }
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Error, "{0}", ex);
            }
        }

        private void EpsonRobotService_RobotDataReveivedEvent(CEpsonRobotData data)
        {
            try
            {
                if (data == null)
                    return;

                if (data.Type == EEpsonRobotData.IOStates)
                    updateIOStates(data);
                else if (data.Type == EEpsonRobotData.POS)
                    updatePosition(data);
                else if (data.Type == EEpsonRobotData.Point)
                    updatePointList(data);
                else if (data.Type == EEpsonRobotData.MIOStates)
                    updateMIOStates(data);
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Error, "{0}", ex);
            }
        }

        private void updateIOStates(CEpsonRobotData data)
        {
            try
            {
                if (data.Type == EEpsonRobotData.IOStates)
                {
                    string[] ioStates = data.Content.Split('|');
                    if (ioStates.Length == 2)
                    {
                        for (int i = 0; i < ioStates[0].Length; i++) IOInputStates[i] = (ioStates[0].Substring(i, 1) == "1");
                        for (int i = 0; i < ioStates[1].Length; i++) IOOutputStates[i] = (ioStates[1].Substring(i, 1) == "1");
                        if (PropertyChanged != null)
                        {
                            PropertyChanged(this, new PropertyChangedEventArgs("IOInputStates"));
                            PropertyChanged(this, new PropertyChangedEventArgs("IOOutputStates"));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Error, "{0}", ex);
            }
        }

        private void updateMIOStates(CEpsonRobotData data)
        {
            try
            {
                if (data.Type == EEpsonRobotData.MIOStates)
                {
                    for (int i = 0; i < data.Content.Length; i++) MIOStates[i] = (data.Content.Substring(i, 1) == "1");
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("MIOStates"));
                    }
                }
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Error, "{0}", ex);
            }
        }

        private void updatePosition(CEpsonRobotData data)
        {
            try
            {
                if (data.Type == EEpsonRobotData.POS)
                {
                    string[] datas = data.Content.Split(':');
                    if (datas.Length == 12)
                    {
                        J1Pos = double.Parse(datas[0]);
                        J2Pos = double.Parse(datas[1]);
                        J3Pos = double.Parse(datas[2]);
                        J4Pos = double.Parse(datas[3]);
                        J5Pos = double.Parse(datas[4]);
                        J6Pos = double.Parse(datas[5]);
                        J1Flag = datas[6];
                        J4Flag = datas[7];
                        J6Flag = datas[8];
                        Hand = datas[9].Contains("1") ? "Righty" : "Lefty";
                        Elbow = datas[10].Contains("1") ? "Above" : "Below";
                        Wrist = datas[11].Contains("1") ? "NoFlip" : "Flip";
                    }
                }
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Error, "{0}", ex);
            }
        }

        private void updatePointList(CEpsonRobotData data)
        {
            try
            {
                if (data.Type != EEpsonRobotData.Point) return;

                var pt = getRobotPoint(data.Sender, data.Content);
                int oIdx = -1;
                for (int i = 0; i < WorkPosList.Count; i++)
                {
                    if (pt.Name.TrimEnd(' ') == string.Format("P{0}", WorkPosList[i].ID))
                    {
                        oIdx = i;
                        break;
                    }
                }
                if (oIdx != -1)
                {
                    ThreadPool.QueueUserWorkItem(delegate
                    {
                        System.Threading.SynchronizationContext.SetSynchronizationContext(new
                            System.Windows.Threading.DispatcherSynchronizationContext(System.Windows.Application.Current.Dispatcher));
                        System.Threading.SynchronizationContext.Current.Post(pl =>
                        {
                            WorkPosList[oIdx] = pt;
                            if (PropertyChanged != null)
                                PropertyChanged(this, new PropertyChangedEventArgs("WorkPosList"));
                        }, null);
                    });
                }
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Error, "{0}", ex);
            }
        }

        private WorkPos getRobotPoint(string rID, string strPt)
        {
            WorkPos pt;
            try
            {
                string pName;
                string strXY;
                string strFlag;
                string strLocal;
                pName = strPt.Substring(0, strPt.IndexOf("="));
                strXY = strPt.Substring(strPt.IndexOf("(") + 1, strPt.IndexOf(")") - strPt.IndexOf("(") - 1);
                strFlag = strPt.Substring(strPt.IndexOf("/") + 1, strPt.LastIndexOf("/") - strPt.IndexOf("/") - 1);
                strLocal = strPt.Substring(strPt.LastIndexOf("/") + 1, 3);
                string[] strXYs = strXY.Split(',');
                string[] strFlags = strFlag.Split('/');
                pt = new WorkPos();                
                pt.Name = pName;
                pt.Local = ushort.Parse(strLocal.TrimEnd(' '));
                if (strXYs.Length == 6 && rID == EEpsonRobotID.R4.ToString())
                {
                    pt.X = float.Parse(strXYs[0]);
                    pt.Y = float.Parse(strXYs[1]);
                    pt.Z = float.Parse(strXYs[2]);
                    pt.U = float.Parse(strXYs[3]);
                    pt.V = float.Parse(strXYs[4]);
                    pt.W = float.Parse(strXYs[5]);
                }
                else if (strXYs.Length >= 4)
                {
                    pt.X = float.Parse(strXYs[0]);
                    pt.Y = float.Parse(strXYs[1]);
                    pt.Z = float.Parse(strXYs[2]);
                    pt.U = float.Parse(strXYs[3]);
                }

                if (strFlags.Length >= 3)
                {
                    pt.Hand = strFlags[0];
                    if (rID == EEpsonRobotID.R4.ToString())
                    {
                        pt.Elbow = strFlags[1];
                        pt.Wrist = strFlags[2];
                    }
                }
                int sPos;
                foreach (var flag in strFlags)
                {
                    sPos = flag.IndexOf("J6F");
                    if (sPos >= 0)
                        pt.J6Flag = ushort.Parse(flag.Substring(sPos + 3));
                    sPos = flag.IndexOf("J4F");
                    if (sPos >= 0)
                        pt.J4Flag = ushort.Parse(flag.Substring(sPos + 3));
                }
            }
            catch (Exception ex)
            {
                pt = null;
                _Logger.Log()(LogSeverityEnum.Error, "{0}", ex);
            }

            return pt;
        }

        public ICommand btnClick
        {
            get
            {
                return new ViewCommand(btnClickResp, btnClickCheck);
            }
        }
        public bool btnClickCheck()
        {
            return true;
        }
        public void btnClickResp(object sender)
        {
            try
            {
                bool ret = false;
                bool gripperSwitchClicked = false;
                TimerEnable(false);
                if (sender is ComboBox)
                {
                    var btn = sender as ComboBox;
                   if (btn.Name == "comboBoxCmd")
                    {
                        selectedCmd = btn.SelectedValue as EpsonRobotConfigCmd;
                        CmdParameter = selectedCmd.ParameterDescript;
                        SaveAsWorkPosEnable = selectedCmd.ToString().Contains(EEpsonRobotCmd.Chip2Imager.ToString()) ? true : false;                            
                    }
                    else if (btn.Name == "comboBoxLocal")
                    {
                        selectedLocal = ushort.Parse(btn.SelectedValue.ToString());
                    }
                }
                else if (sender is Button)
                {
                    #region button_click_event
                    var btn = sender as Button;
                    if (btn.Name == "buttonExcuteCmd" && selectedCmd != null)
                    {
                        string strCmd = selectedCmd.ToString();                        
                        if (CmdParameter != string.Empty) strCmd = string.Format("{0}:{1}", selectedCmd, cmdParameter);
                        if (MessageBoxResult.No == MessageBox.Show(string.Format("Do you want to excute cmd[{0}]?", strCmd), "Question", MessageBoxButton.YesNo, MessageBoxImage.Question)) return;

                        string strMsg;
                        Dictionary<string, string> cmdQ = new Dictionary<string, string> { { strCmd, null } };
                        int errCode = VEpsonRobotService.Get.ConnectedCmdSocketDict[VEpsonRobotService.Get.RobotID2IPDict[robotID]].sentCmd2Robot(ref cmdQ);
                        if (errCode != 0)
                            strMsg = string.Format("Excute Cmd[{0}] failed.ErrorCode={1}", strCmd, errCode);
                        else
                            strMsg = string.Format("Excute Cmd[{0}] successed.CmdRet={1}", strCmd, cmdQ[strCmd]);
                        MessageBox.Show(strMsg, "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else if (btn.Name == "buttonExcuteRobotScript")
                    {
                        VEpsonRobotService.Get.ConnectedDataSocketDict[VEpsonRobotService.Get.RobotID2IPDict[robotID]].sentCmd2Robot(RobotScript);
                    }
                    else if (btn.Name == "buttonRefreshPointList")
                    {
                        refreshPointList();
                    }
                    else if (btn.Name == "buttonTechWorkPos")
                    {
                        if (SelectedPosId < 0) return;
                        if (MessageBoxResult.No == MessageBox.Show(string.Format("Do you want to teach current position to {0}@{1}?", SelectedPosId, selectedLocal), "Question", MessageBoxButton.YesNo, MessageBoxImage.Question)) return;

                        ret = VEpsonRobotService.Get.TeachWorkPos(robotID, (ushort)SelectedPosId, selectedLocal);
                        string strMsg = string.Format("Teach point[{0}]", SelectedPosId);
                        strMsg += ret ? "success" : "failed";
                        MessageBox.Show(strMsg, "Information", MessageBoxButton.OK, MessageBoxImage.Information);   
                    }
                    else if (btn.Name == "buttonSaveAsWorkPos")
                    {
                        ushort imagerIdx;
                        if (CmdParameter.Length > 0 && ushort.TryParse(CmdParameter.Substring(CmdParameter.Length - 1), out imagerIdx))
                        {
                            ushort tPointIdx = (ushort)(IMAGER_BASE_POSID + imagerIdx);
                            string strMsg = string.Format("SaveAsWorkPos[{0}->{1}]", TEACH_TEMP_POSID, tPointIdx);
                            if (MessageBoxResult.No == MessageBox.Show(string.Format("Do you want to {0}?", strMsg), "Question", MessageBoxButton.YesNo, MessageBoxImage.Question)) return;

                            ret = VEpsonRobotService.Get.SaveAsWorkPos(robotID, TEACH_TEMP_POSID, tPointIdx);
                            MessageBox.Show((strMsg += ret ? "success" : "failed"), "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                    }
                    else if (btn.Name == "buttonGoHome")
                    {
                        if (MessageBoxResult.Yes == MessageBox.Show(string.Format("Do you want to excute GoHome?", SelectedPosId, selectedLocal), "Question", MessageBoxButton.YesNo, MessageBoxImage.Question))
                        {
                            ret = VEpsonRobotService.Get.GoHome(robotID);
                            string strMsg = ret ? "GoHome success" : "GoHome failed";
                            MessageBox.Show(strMsg, "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                        }                        
                    }
                    else if (btn.Name == "buttonGripperSwitch")
                    {
                        VEpsonRobotService.Get.GripperSwitch(robotID, gripperSwitchClicked);
                        gripperSwitchClicked = !gripperSwitchClicked;
                    }
                    #region move_step
                    else if (btn.Name == "buttonJ1Up")
                    {
                            VEpsonRobotService.Get.StepMove(robotID, 1, selectedLocal, StepDist);
                    }
                    else if (btn.Name == "buttonJ1Dn")
                    {
                            VEpsonRobotService.Get.StepMove(robotID, 1, selectedLocal, StepDist * -1);
                    }
                    else if (btn.Name == "buttonJ2Up")
                    {
                            VEpsonRobotService.Get.StepMove(robotID, 2, selectedLocal, StepDist);
                    }
                    else if (btn.Name == "buttonJ2Dn")
                    {
                            VEpsonRobotService.Get.StepMove(robotID, 2, selectedLocal, StepDist * -1);
                    }
                    else if (btn.Name == "buttonJ3Up")
                    {
                            VEpsonRobotService.Get.StepMove(robotID, 3, selectedLocal, StepDist);
                    }
                    else if (btn.Name == "buttonJ3Dn")
                    {
                            VEpsonRobotService.Get.StepMove(robotID, 3, selectedLocal, StepDist * -1);
                    }
                    else if (btn.Name == "buttonJ4Up")
                    {
                            VEpsonRobotService.Get.StepMove(robotID, 4, selectedLocal, StepDist);
                    }
                    else if (btn.Name == "buttonJ4Dn")
                    {
                            VEpsonRobotService.Get.StepMove(robotID, 4, selectedLocal, StepDist * -1);
                    }
                    else if (btn.Name == "buttonJ5Up")
                    {
                            VEpsonRobotService.Get.StepMove(robotID, 5, selectedLocal, StepDist);
                    }
                    else if (btn.Name == "buttonJ5Dn")
                    {
                            VEpsonRobotService.Get.StepMove(robotID, 5, selectedLocal, StepDist * -1);
                    }
                    else if (btn.Name == "buttonJ6Up")
                    {
                            VEpsonRobotService.Get.StepMove(robotID, 6, selectedLocal, StepDist);
                    }
                    else if (btn.Name == "buttonJ6Dn")
                    {
                            VEpsonRobotService.Get.StepMove(robotID, 6, selectedLocal, StepDist * -1);
                    }
                    #endregion move_step
                    else if (btn.Name == "buttonStart")
                    {
                        robotRemoter.Start();
                    }
                    else if (btn.Name == "buttonStop")
                    {
                        robotRemoter.Stop();
                    }
                    else if (btn.Name == "buttonPause")
                    {
                        robotRemoter.Pause();
                    }
                    else if (btn.Name == "buttonContinue")
                    {
                        robotRemoter.Continue();
                    }
                    else if (btn.Name == "buttonReset")
                    {
                        robotRemoter.Reset();
                    }
                    #endregion button_click_event
                }
                else if (sender is CheckBox)
                {
                    var btn = sender as CheckBox;
                    if (btn.Name.IndexOf("OUT") > 0)
                    {
                        ushort outIdx = ushort.Parse(btn.Name.Substring(btn.Name.IndexOf("OUT") + 3));
                        bool enable = (btn.IsChecked == true);
                        if (VEpsonRobotService.Get.RobotID2IPDict.ContainsKey(robotID))
                        {
                            VEpsonRobotService.Get.SwitchRobotOutput(VEpsonRobotService.Get.RobotID2IPDict[robotID], outIdx, enable);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Error, "{0}", ex);
            }
            finally
            {
                TimerEnable(true);
            }
        }

        CEpsonRobotRemoter robotRemoter = new CEpsonRobotRemoter(EpsonRobotConfig.Instance.RobotIP, EpsonRobotConfig.Instance.ControlPort);

        private void refreshPointList()
        {
            List<ushort> pIds = new List<ushort>();
            foreach (var wp in EpsonRobotConfig.Instance.WorkPos) pIds.Add(wp.ID);
            Task.Factory.StartNew(() =>
            {
                foreach (var pId in pIds)
                {
                    VEpsonRobotService.Get.GetPoint(robotID, pId);
                    Thread.Sleep(200);
                }
            });
        }

        private void refreshParameterList()
        {
            List<string> pIds = new List<string>();
            foreach (var wp in EpsonRobotConfig.Instance.MotionParameter) pIds.Add(wp.Name);
            Task.Factory.StartNew(() =>
            {
                foreach (var pId in pIds)
                {
                    string mValue = VEpsonRobotService.Get.GetParameter(robotID, pId);
                    if (mValue != string.Empty) UpdateParameterList(pId, mValue);
                    Thread.Sleep(100);
                }
            });
        }
    }
}
