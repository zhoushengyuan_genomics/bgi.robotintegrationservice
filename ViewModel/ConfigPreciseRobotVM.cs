﻿using BGI.Common.Logging;
using BGI.RobotIntegrationService.PreciseRobot;
using BGI.RobotIntegrationService.Workflow;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace BGI.RobotIntegrationService.ViewModel
{
    class ConfigPreciseRobotVM : INotifyPropertyChanged
    {
        private PF400Robot robot;
        private Profile selectedProfile;
        private WorkflowGroupWorkflow crtWorkflow;
        

        public int SelectedWorkflowMotionIdx;

        public event PropertyChangedEventHandler PropertyChanged;
        private Logger _Logger = LogMgr.GetLogger("ConfigPreciseRobotVM");
        public ConfigPreciseRobotVM()
        {
            ProfileList = new ObservableCollection<Profile>();
            WorkflowList = new ObservableCollection<WorkflowGroupWorkflow>();
            foreach (var pf in WorkflowGroup.Instance.Profile) ProfileList.Add(pf);
            foreach (var wf in WorkflowGroup.Instance.Workflow) WorkflowList.Add(wf);

            robot = new PF400Robot();
            robot.RobotStatusChanged += Robot_RobotStatusChanged;
            Task.Run(() =>
            {
                while (robot.Connected)// && robot.RobotStatus == ERobotStatus.Idle
                {
                    JCoordinate newJ = robot.GetCurrentJCoordinate();
                    if (newJ != null)
                    {
                        PosJ1 = string.Format("J1:{0}", newJ.Axis1);
                        PosJ2 = string.Format("J2:{0}", newJ.Axis2);
                        PosJ3 = string.Format("J3:{0}", newJ.Axis3);
                        PosJ4 = string.Format("J4:{0}", newJ.Axis4);
                        PosJ5 = string.Format("J5:{0}", newJ.Axis5);
                        PosJ6 = string.Format("J6:{0}", newJ.Axis6);
                    }

                    Thread.Sleep(1000);
                }
            });
        }

        private void Robot_RobotStatusChanged(ERobotStatus robotStatus, string msg)
        {
            string strColor;
            if (robotStatus == ERobotStatus.Idle)
                strColor = "Yellow";
            else if (robotStatus == ERobotStatus.Busy)
                strColor = "Green";
            else
                strColor = "Red";
            
            RobotStatusColor = strColor;
        }

        #region ViewDataBinding
        private string robotIP;
        private ushort robotPort;
        private ushort speed;
        private ushort speed2;
        private ushort accel;
        private ushort decel;
        private float accelRamp;
        private float decelRamp;
        private short inRange;
        private bool straight;        

        public float StepDist
        {
            get
            {
                return WorkflowGroup.Instance.RobotStepDist; 
            }

            set
            {
                WorkflowGroup.Instance.RobotStepDist = value;
                robot.ChangeStepDist(value);
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("StepDist"));
            }
        }
        public ushort PercentMaxSpeed
        {
            get
            {
                return WorkflowGroup.Instance.PercentMaxSpeed;
            }

            set
            {
                robot.PercentOfMaxVelocity = value;
                WorkflowGroup.Instance.PercentMaxSpeed = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("PercentMaxSpeed"));
            }
        }

        private string _RobotStatusMsg;
        public string RobotStatusMsg
        {
            get
            {
                return _RobotStatusMsg;
            }

            set
            {
                _RobotStatusMsg = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("RobotStatusMsg"));
            }
        }
        private string _RobotStatusColor;
        public string RobotStatusColor
        {
            get
            {
                return _RobotStatusColor;
            }

            set
            {
                _RobotStatusColor = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("RobotStatusColor"));
            }
        }
        public string RobotIP
        {
            get
            {
                return WorkflowGroup.Instance.RobotIP;
            }

            set
            {
                WorkflowGroup.Instance.RobotIP = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("RobotIP"));
            }
        }
        public ushort RobotPort
        {
            get
            {
                return WorkflowGroup.Instance.Port;
            }

            set
            {
                WorkflowGroup.Instance.Port = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("RobotPort"));
            }
        }
        public ushort Speed
        {
            get
            {
                return speed;
            }

            set
            {
                speed = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("Speed"));
            }
        }
        public ushort Speed2
        {
            get
            {
                return speed2;
            }

            set
            {
                speed2 = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("Speed2"));
            }
        }
        public ushort Accel
        {
            get
            {
                return accel;
            }

            set
            {
                accel = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("Accel"));
            }
        }
        public ushort Decel
        {
            get
            {
                return decel;
            }

            set
            {
                decel = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("Decel"));
            }
        }
        public float AccelRamp
        {
            get
            {
                return accelRamp;
            }

            set
            {
                accelRamp = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("AccelRamp"));
            }
        }
        public float DecelRamp
        {
            get
            {
                return decelRamp;
            }

            set
            {
                decelRamp = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("DecelRamp"));
            }
        }
        public short InRange
        {
            get
            {
                return inRange;
            }

            set
            {
                inRange = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("InRange"));
            }
        }
        public bool Straight
        {
            get
            {
                return straight;
            }

            set
            {
                straight = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("Straight"));
            }
        }

        private string posJ1 = "J1:0.0";
        public string PosJ1
        {
            get
            {
                return posJ1;
            }

            set
            {
                posJ1 = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("PosJ1"));
            }
        }
        private string posJ2 = "J2:0.0";
        public string PosJ2
        {
            get
            {
                return posJ2;
            }

            set
            {
                posJ2 = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("PosJ2"));
            }
        }
        private string posJ3 = "J3:0.0";
        public string PosJ3
        {
            get
            {
                return posJ3;
            }

            set
            {
                posJ3 = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("PosJ3"));
            }
        }
        private string posJ4 = "J4:0.0";
        public string PosJ4
        {
            get
            {
                return posJ4;
            }

            set
            {
                posJ4 = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("PosJ4"));
            }
        }
        private string posJ5 = "J5:0.0";
        public string PosJ5
        {
            get
            {
                return posJ5;
            }

            set
            {
                posJ5 = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("PosJ5"));
            }
        }
        private string posJ6 = "J6:0.0";
        public string PosJ6
        {
            get
            {
                return posJ6;
            }

            set
            {
                posJ6 = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("PosJ6"));
            }
        }        
        public ObservableCollection<Profile> ProfileList { get; set; }
        public ObservableCollection<WorkflowGroupWorkflow> WorkflowList { get; set; }
        public ObservableCollection<WorkflowGroupWorkflowMotion> WorkflowMotionList { get; set; }
        

        #endregion ViewDataBinding

        public ICommand btnClick
        {
            get
            {
                return new ViewCommand(btnClickResp, btnClickCheck);
            }
        }
        public bool btnClickCheck()
        {
            return true;
        }

        public void btnClickResp(object sender)
        {
            try
            {
                if (sender is ComboBox)
                {
                    var btn = sender as ComboBox;
                    if (btn.Name == "comboBoxWorkflow")
                    {
                        crtWorkflow = (WorkflowGroupWorkflow)btn.SelectedValue;
                        updateWorkflowMotionList(crtWorkflow);
                    }
                    else if (btn.Name == "comboBoxProfile")
                    {
                        selectedProfile = btn.SelectedValue as Profile;
                        Speed = selectedProfile.Speed;
                        Speed2 = selectedProfile.Speed2;
                        Accel = selectedProfile.Accel;
                        Decel = selectedProfile.Decel;
                        AccelRamp  = selectedProfile.AccelRamp;
                        DecelRamp = selectedProfile.DecelRamp;
                        Straight = selectedProfile.Straight;
                        InRange = selectedProfile.InRange;
                    }

                }
                else if (sender is Button)
                {                    
                    #region ButtonClick_EventHandler
                    var btn = sender as Button;
                    if (btn.Name == "buttonConnect")
                    {
                        robot.Connect(WorkflowGroup.Instance.RobotIP,WorkflowGroup.Instance.Port);
                        if (robot.Connected)
                        {
                            WorkflowGroup.Instance.PercentMaxSpeed = robot.PercentOfMaxVelocity;
                            if (PropertyChanged != null)
                                PropertyChanged(this, new PropertyChangedEventArgs("PercentMaxSpeed"));
                        }
                    }
                    else if (btn.Name == "buttonDisconnect")
                    {
                        robot.DisConnect();
                    }
                    else if (btn.Name == "buttonPowerOff")
                    {
                        robot.SetPower(false);
                    }
                    else if (btn.Name == "buttonPowerON")
                    {
                        robot.SetPower(true);
                    }
                    else if (btn.Name == "buttonTeachWorkpos")
                    {
                        if (SelectedWorkflowMotionIdx == -1)
                        {
                            MessageBox.Show("please select the motion which you want to teach...");
                            return;
                        }

                        teachWorkflowMotionWorkpos(SelectedWorkflowMotionIdx);
                    }
                    else if (btn.Name == "buttonExcuteWorkflow")
                    {
                        if (!robot.AutoRunReady || crtWorkflow == null) return;

                        var ret = robot.ExcuteWorkFlow(crtWorkflow.Name);
                        MessageBox.Show(string.Format("Excute workflow[{0}] {1}", crtWorkflow.Name, (ret.ErrorCode == 0 ? true : false)));
                    }
                    else if (btn.Name == "buttonGoCenter")
                    {
                        if (!robot.Connected) return;

                        var p = WorkflowGroup.Instance.GetPosition("SafePosition");
                        robot.MoveTo(new JCoordinate(p.J1, p.J2, p.J3, p.J4, p.J5, p.J6));
                    }
                    else if (btn.Name == "buttonSet")
                    {
                        if (!robot.Connected || selectedProfile == null) return;

                        robot.SetProfile(selectedProfile);
                    }
                    else if (btn.Name == "buttonGet")
                    {
                        if (!robot.Connected || selectedProfile == null) return;

                        if (robot.GetProfile(ref selectedProfile).ReplyCode == RobotReply.ReplyCodeEnum.OK)
                        {
                            Speed = selectedProfile.Speed;
                            Speed2 = selectedProfile.Speed2;
                            Accel = selectedProfile.Accel;
                            Decel = selectedProfile.Decel;
                            AccelRamp = selectedProfile.AccelRamp;
                            DecelRamp = selectedProfile.DecelRamp;
                            Straight = selectedProfile.Straight;
                            InRange = selectedProfile.InRange;
                        }
                        WorkflowGroup.UpdateWorkflowGroupDefine();
                    }
                    else if (btn.Name == "buttonJ1Up" && robot.RobotStatus == ERobotStatus.Idle)
                        robot.PositiveStepMove(ERobotAxis.Axis1);
                    else if (btn.Name == "buttonJ1Dn" && robot.RobotStatus == ERobotStatus.Idle)
                        robot.NegativeStepMove(ERobotAxis.Axis1);
                    else if (btn.Name == "buttonJ2Up" && robot.RobotStatus == ERobotStatus.Idle)
                        robot.PositiveStepMove(ERobotAxis.Axis2);
                    else if (btn.Name == "buttonJ2Dn" && robot.RobotStatus == ERobotStatus.Idle)
                        robot.NegativeStepMove(ERobotAxis.Axis2);
                    else if (btn.Name == "buttonJ3Up" && robot.RobotStatus == ERobotStatus.Idle)
                        robot.PositiveStepMove(ERobotAxis.Axis3);
                    else if (btn.Name == "buttonJ3Dn" && robot.RobotStatus == ERobotStatus.Idle)
                        robot.NegativeStepMove(ERobotAxis.Axis3);
                    else if (btn.Name == "buttonJ4Up" && robot.RobotStatus == ERobotStatus.Idle)
                        robot.PositiveStepMove(ERobotAxis.Axis4);
                    else if (btn.Name == "buttonJ4Dn" && robot.RobotStatus == ERobotStatus.Idle)
                        robot.NegativeStepMove(ERobotAxis.Axis4);
                    else if (btn.Name == "buttonJ5Up" && robot.RobotStatus == ERobotStatus.Idle)
                        robot.PositiveStepMove(ERobotAxis.Axis5);
                    else if (btn.Name == "buttonJ5Dn" && robot.RobotStatus == ERobotStatus.Idle)
                        robot.NegativeStepMove(ERobotAxis.Axis5);
                    else if (btn.Name == "buttonJ6Up" && robot.RobotStatus == ERobotStatus.Idle)
                        robot.PositiveStepMove(ERobotAxis.Axis6);
                    else if (btn.Name == "buttonJ6Dn" && robot.RobotStatus == ERobotStatus.Idle)
                        robot.NegativeStepMove(ERobotAxis.Axis6);
                    #endregion ButtonClick_EventHandler
                }
                else if (sender is CheckBox)
                {
                    var btn = sender as CheckBox;
                    Console.WriteLine(btn.Name);
                    bool ischecked;
                    ERobotAxis optAxis;
                    if (isLockAxisOperation(btn, out optAxis, out ischecked))
                    {
                        // TBD
                        //if (ischecked)
                        //{
                        //    robot.LockAxis(optAxis);                            
                        //}
                        //else
                        //{
                        //    robot.FreeAxis(optAxis);
                        //}
                    }
                }
                else if (sender is RadioButton)
                {
                    var btn = sender as RadioButton;
                    Console.WriteLine(btn.Name);                    
                }
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Error, "{0}", ex);
            }
        }

        private bool isLockAxisOperation(CheckBox btn, out ERobotAxis optAxis, out bool enable)
        {
            enable = false;
            optAxis = ERobotAxis.Unknow;
            if (btn.Name == "CheckBoxJ1Enable")
            {
                optAxis = ERobotAxis.Axis1;
            }
            else if (btn.Name == "CheckBoxJ2Enable")
            {
                optAxis = ERobotAxis.Axis2;
            }
            else if (btn.Name == "CheckBoxJ3Enable")
            {
                optAxis = ERobotAxis.Axis3;
            }
            else if (btn.Name == "CheckBoxJ4Enable")
            {
                optAxis = ERobotAxis.Axis4;
            }
            else if (btn.Name == "CheckBoxJ5Enable")
            {
                optAxis = ERobotAxis.Axis5;
            }
            else if (btn.Name == "CheckBoxJ6Enable")
            {
                optAxis = ERobotAxis.Axis6;
            }
            if (btn.IsChecked.HasValue && (bool)btn.IsChecked) enable = true;

            return (optAxis != ERobotAxis.Unknow);
        }

        private void updateWorkflowMotionList(WorkflowGroupWorkflow wf)
        {
            try
            {
                WorkflowMotionList = new ObservableCollection<WorkflowGroupWorkflowMotion>();
                foreach (var mo in wf.Motion)
                {
                    WorkflowMotionList.Add(mo);
                }

                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("WorkflowMotionList"));
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Error, "{0}", ex);
            }
        }

        private void teachWorkflowMotionWorkpos(int motionIdx)
        {
            try
            {
                crtWorkflow.Motion[motionIdx].Position.J1 = float.Parse(PosJ1.Substring(PosJ1.IndexOf(":") + 1));
                crtWorkflow.Motion[motionIdx].Position.J2 = float.Parse(PosJ2.Substring(PosJ2.IndexOf(":") + 1));
                crtWorkflow.Motion[motionIdx].Position.J3 = float.Parse(PosJ3.Substring(PosJ3.IndexOf(":") + 1));
                crtWorkflow.Motion[motionIdx].Position.J4 = float.Parse(PosJ4.Substring(PosJ4.IndexOf(":") + 1));
                crtWorkflow.Motion[motionIdx].Position.J5 = float.Parse(PosJ5.Substring(PosJ5.IndexOf(":") + 1));
                crtWorkflow.Motion[motionIdx].Position.J6 = float.Parse(PosJ6.Substring(PosJ6.IndexOf(":") + 1));
                updateWorkflowMotionList(crtWorkflow);
                WorkflowGroup.UpdateWorkflowGroupDefine();
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Error, "{0}", ex);
            }
        }

        public void UpdateWorkflowMotionWorkpos(int motionIdx,int jIdx, float jValue)
        {
            try
            {
                if (jIdx < 0 || jIdx > 5) return;

                switch(jIdx)
                {
                    case 0:
                        crtWorkflow.Motion[motionIdx].Position.J1 = jValue;
                        break;
                    case 1:
                        crtWorkflow.Motion[motionIdx].Position.J2 = jValue;
                        break;
                    case 2:
                        crtWorkflow.Motion[motionIdx].Position.J3 = jValue;
                        break;
                    case 3:
                        crtWorkflow.Motion[motionIdx].Position.J4 = jValue;
                        break;
                    case 4:
                        crtWorkflow.Motion[motionIdx].Position.J5 = jValue;
                        break;
                    case 5:
                        crtWorkflow.Motion[motionIdx].Position.J6 = jValue;
                        break;
                }
                WorkflowGroup.UpdateWorkflowGroupDefine();
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Error, "{0}", ex);
            }
        }
    }
}
