﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace BGI.RobotIntegrationService.ViewModel
{
    class ViewCommand : ICommand
    {
        private Action<object> whatToExecute;
        private Func<bool> whenToExecute;
        public event EventHandler CanExecuteChanged;

        public ViewCommand(Action<object> what, Func<bool> when)
        {
            whatToExecute = what;
            whenToExecute = when;
        }

        public bool CanExecute(object parameter)
        {
            return whenToExecute();
        }

        public void Execute(object parameter)
        {
            whatToExecute(parameter);
        }
    }
}
