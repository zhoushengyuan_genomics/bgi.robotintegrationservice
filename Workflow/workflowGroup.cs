﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

namespace BGI.RobotIntegrationService.Workflow
{
    public partial class WorkflowGroup
    {
        private static string workflowDefineFileName = "WorkflowGroup.xml"; 
        private static WorkflowGroup instance;

        public static WorkflowGroup Instance
        {
            get 
            { 
                if (instance == null)
                    instance = LoadWorkflowGroupDefine();

                return instance; 
            }
        }
        
        public static void UpdateWorkflowGroupDefine()
        {
            try
            {
                FileStream stream = new FileStream(workflowDefineFileName, FileMode.Create);

                XmlSerializer xmlserilize = new XmlSerializer(typeof(WorkflowGroup));

                xmlserilize.Serialize(stream, WorkflowGroup.instance);

                stream.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private static WorkflowGroup LoadWorkflowGroupDefine()
        {
            WorkflowGroup wfGroup = null;
            try
            {
                if (File.Exists(workflowDefineFileName))
                {
                    XmlDocument xml = new XmlDocument();
                    xml.Load(workflowDefineFileName);
                    string xmltxt = xml.OuterXml;
                    wfGroup = WorkflowGroup.Parse(xmltxt);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            return wfGroup;
        }

        public static WorkflowGroup Parse(string xmlStr)
        {
            using (StringReader sr = new StringReader(xmlStr))
            {
                XmlReader xr = new XmlTextReader(sr);
                return Parse(xr);
            }
        }

        private static WorkflowGroup Parse(XmlReader xr)
        {
            XmlSerializer xs = new XmlSerializer(typeof(WorkflowGroup));

            if (!xs.CanDeserialize(xr))
                return null;

            object tmp = xs.Deserialize(xr);
            WorkflowGroup result = tmp as WorkflowGroup;
            return result;
        }

        public Position GetPosition(string pName)
        {
            foreach(var p in Position)
            {
                if (p.Name == pName) return p;
            }

            return null;
        }

        public WorkflowGroupWorkflow GetWorkflow(string wfName)
        {
            foreach (var wf in Workflow)
            {
                if (wf.Name == wfName) return wf;
            }

            return null;
        }

        public void AddWorkflow(WorkflowGroupWorkflow wf)
        {
            try
            {
                lock (instance.Workflow)
                {
                    WorkflowGroupWorkflow[] wfs = new WorkflowGroupWorkflow[instance.Workflow.Length];
                    instance.Workflow.CopyTo(wfs, 0);
                    instance.Workflow = new WorkflowGroupWorkflow[wfs.Length + 1];
                    wfs.CopyTo(instance.Workflow, 0);
                    instance.Workflow[wfs.Length] = wf;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RemoveWorkflow(WorkflowGroupWorkflow wf)
        {
            try
            {
                lock (instance.Workflow)
                {
                    List<WorkflowGroupWorkflow> wfList = instance.Workflow.ToList();
                    wfList.Remove(wf);
                    instance.Workflow = new WorkflowGroupWorkflow[wfList.Count];
                    instance.Workflow = wfList.ToArray();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

    public partial class WorkflowGroupWorkflow
    {       
        public void RemoveAction(WorkflowGroupWorkflowMotion action)
        {
            try
            {
                lock (this.Motion)
                {
                    List<WorkflowGroupWorkflowMotion> actionList = this.Motion.ToList();
                    actionList.Remove(action);
                    this.Motion = new WorkflowGroupWorkflowMotion[actionList.Count];
                    this.Motion = actionList.ToArray();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddAction(WorkflowGroupWorkflowMotion action)
        {
            try
            {
                if (this.Motion == null)
                {
                    this.Motion = new WorkflowGroupWorkflowMotion[] { action };
                }
                else
                {
                    lock (this.Motion)
                    {
                        List<WorkflowGroupWorkflowMotion> actionList = this.Motion.ToList();
                        actionList.Add(action);
                        this.Motion = new WorkflowGroupWorkflowMotion[actionList.Count];
                        this.Motion = actionList.ToArray();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public override string ToString()
        {
            return this.Name;
        }
    }

    public partial class Profile
    {
        public static Profile ParseProfile(string strProfile)
        {
            Profile pf = null;
            string[] strArr = strProfile.Split(' ');
            if (strArr.Length == 9)
            {
                pf = new Profile();
                pf.Idx = ushort.Parse(strArr[0]);
                pf.Speed = ushort.Parse(strArr[1]);
                pf.Speed2 = ushort.Parse(strArr[2]);
                pf.Accel = ushort.Parse(strArr[3]);
                pf.Decel = ushort.Parse(strArr[4]);
                pf.AccelRamp = float.Parse(strArr[5]);
                pf.DecelRamp = float.Parse(strArr[6]);
                pf.InRange = short.Parse(strArr[7]);
                pf.Straight = (short.Parse(strArr[8]) == 0 ? false : true);
            }

            return pf;
        }
        public string ProfileCommandString
        {
            get
            {
                string cmdDataStr = String.Format("{0} {1} {2} {3} {4} {5} {6} {7} {8}",
                    Idx, Speed, Speed2, Accel, Decel, AccelRamp, DecelRamp, InRange, Straight ? -1 : 0);

                return cmdDataStr;
            }
        }

        public override string ToString()
        {
            return this.Idx.ToString();
        }
    }

    public partial class WorkflowGroupWorkflowMotion
    {
        public void IniAction(ushort actionIdx,
                                string actionName,
                                bool waitForEom,
                                ushort profile,
                                Position position)
        {
            try
            {
                this.Idx = actionIdx;
                this.Name = actionName;
                this.WaitForEom = waitForEom;
                this.Profile = profile;
                this.Position = position;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public override string ToString()
        {
            return string.Format("{0}({1})", this.Idx, this.Name);
        }

        #region Fields to binding View's control        
        [System.Xml.Serialization.XmlIgnore]
        public ushort VIdx
        {
            get
            {
                return Idx;
            }

            set
            {
                Idx = value;
            }
        }
        [System.Xml.Serialization.XmlIgnore]
        public string VName
        {
            get
            {
                return Name;
            }

            set
            {
                Name = value;
            }
        }
        [System.Xml.Serialization.XmlIgnore]
        public ushort VProfile
        {
            get
            {
                return Profile;
            }

            set
            {
                Profile = value;
            }
        }
        [System.Xml.Serialization.XmlIgnore]
        public bool VWaitForEom
        {
            get
            {
                return WaitForEom;
            }

            set
            {
                WaitForEom = value;
            }
        }
        [System.Xml.Serialization.XmlIgnore]
        public float VJ1
        {
            get
            {
                return Position.J1;
            }

            set
            {
                Position.J1 = value;
            }
        }
        [System.Xml.Serialization.XmlIgnore]
        public float VJ2
        {
            get
            {
                return Position.J2;
            }

            set
            {
                Position.J2 = value;
            }
        }
        [System.Xml.Serialization.XmlIgnore]
        public float VJ3
        {
            get
            {
                return Position.J3;
            }

            set
            {
                Position.J3 = value;
            }
        }
        [System.Xml.Serialization.XmlIgnore]
        public float VJ4
        {
            get
            {
                return Position.J4;
            }

            set
            {
                Position.J4 = value;
            }
        }
        [System.Xml.Serialization.XmlIgnore]
        public float VJ5
        {
            get
            {
                return Position.J5;
            }

            set
            {
                Position.J5 = value;
            }
        }
        [System.Xml.Serialization.XmlIgnore]
        public float VJ6
        {
            get
            {
                return Position.J6;
            }

            set
            {
                Position.J6 = value;
            }
        }
        
        #endregion
    }
}
